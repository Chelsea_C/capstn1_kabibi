﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Canvas))]
    public class PauseScreen : MonoBehaviour
    {
        public GameObject pausePanel;
        public GameObject controlPanel;
        public Image backgroundOverlay;

        void Start()
        {
            LevelManager.instance.pauseScreen = this.GetComponent<Canvas>();
            gameObject.SetActive(false);
        }

        void Update()
        {
            if (LevelManager.instance.pauseScreen == null)
                LevelManager.instance.pauseScreen = this.GetComponent<Canvas>();
        }

        public void OnPause()
        {
            backgroundOverlay.gameObject.SetActive(true);
            pausePanel.GetComponent<Animator>().SetBool("GamePaused", true);
            controlPanel.GetComponent<Animator>().SetBool("GamePaused", true);
        }

        public void OnResume()
        {
            backgroundOverlay.gameObject.SetActive(false);
            pausePanel.GetComponent<Animator>().SetBool("GamePaused", false);
            controlPanel.GetComponent<Animator>().SetBool("GamePaused", false);
            StartCoroutine("Deactivate");
            
        }

        IEnumerator Deactivate()
        {
            yield return new WaitForSeconds(1f);
            gameObject.SetActive(false);
        }


    }
}
