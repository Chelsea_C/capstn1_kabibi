﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Canvas))]
    public class EndScreen : MonoBehaviour
    {
        public Text best_score_label;
        public Text best_score_value;
        public Text new_best_score_label;

        int bestScore;
        bool trigger;

        void Start()
        {
            LevelManager.instance.levelEnd = this.GetComponent<Canvas>();
            gameObject.SetActive(false);
            trigger = false;
            
        }

        void Update()
        {
            if(!trigger)
            {
                bestScore = LevelManager.instance.currentLevel.data.HIGH_SCORE;
                trigger = true;
            }
            if (LevelManager.instance.levelEnd == null)
                LevelManager.instance.levelEnd = this.GetComponent<Canvas>();

        }

        public void ManualUpdate()
        {
            var level = LevelManager.instance;
            if (level.score < bestScore) // if score is lower than best score, display best score under score
            {
                best_score_label.gameObject.SetActive(true);
                best_score_value.text = level.score.ToString();
                best_score_value.gameObject.SetActive(true);

                new_best_score_label.gameObject.SetActive(false);
            }
            else // if score is new best
            {
                best_score_label.gameObject.SetActive(false);
                best_score_value.gameObject.SetActive(false);

                best_score_value.text = level.currentLevel.data.HIGH_SCORE.ToString();
                new_best_score_label.gameObject.SetActive(true);
            }
        }
    }
}
