﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Canvas))]
    public class HudScreen : MonoBehaviour
    {
        void Start()
        {
            LevelManager.instance.hud = this.GetComponent<Canvas>();
        }

        void Update()
        {
            if (LevelManager.instance.hud == null)
                LevelManager.instance.hud = this.GetComponent<Canvas>();
        }
    }
}
