﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionScreen : MonoBehaviour
{
    public List<InstructionPanel> panels;
    public int currentPanel = 0;
    public Button previous;
    public Button next;
    public Button end;
    public Button skip;
    public Urbana.LevelData level;

    void Start()
    {
        InstructionPanel[] iPanels = GetComponentsInChildren<InstructionPanel>();
        foreach (InstructionPanel ip in iPanels)
            panels.Add(ip);

        previous.onClick.AddListener(GoToPreviousPanel);
        next.onClick.AddListener(GoToNextPanel);
        end.onClick.AddListener(GoToLevel);
        skip.onClick.AddListener(GoToLevel);
        skip.gameObject.SetActive(false);
    }
    
    void Update()
    {
        DisplayCurrentPanel();

        if(currentPanel == 0)
            previous.gameObject.SetActive(false);
        else
            previous.gameObject.SetActive(true);

        if (currentPanel == panels.Count - 1) // if last panel
        {
            end.gameObject.SetActive(true);
            next.gameObject.SetActive(false);
            if (level.InstructionsCompleted)
                skip.gameObject.SetActive(false);
        }
        else
        {
            end.gameObject.SetActive(false);
            next.gameObject.SetActive(true);
            if(level.InstructionsCompleted)
                skip.gameObject.SetActive(true);
        }
    }

    void DisplayCurrentPanel()
    {
        for (int i = 0; i < panels.Count; i++)
        {
            if (i == currentPanel)
                panels[i].gameObject.SetActive(true);
            else
                panels[i].gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (currentPanel + 1 == panels.Count)
                GoToLevel();
            else
                currentPanel++;
        }
    }

    public void GoToPreviousPanel()
    {
        if (currentPanel == 0)
            return;
        currentPanel--;
    }

    public void GoToNextPanel()
    {
        if (currentPanel == panels.Count - 1)
            return;
        currentPanel++;
    }

    public void GoToLevel()
    {
        level.InstructionsCompleted = true;
        Initiate.Fade(level.SCENE_NAME, Color.black, 2f);
    }
}
