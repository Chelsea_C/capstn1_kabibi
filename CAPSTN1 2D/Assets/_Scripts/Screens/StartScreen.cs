﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Canvas))]
    public class StartScreen : MonoBehaviour
    {
        void Start()
        {
            LevelManager.instance.levelStart = this.GetComponent<Canvas>();
        }

        void Update()
        {
            if (LevelManager.instance.levelStart == null)
                LevelManager.instance.levelStart = this.GetComponent<Canvas>();
        }
    }
}
