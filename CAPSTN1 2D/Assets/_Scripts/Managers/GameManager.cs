﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    public enum GAME_STATE
    {
        PLAY,
        PAUSE
    }

    public enum GAME_SPEED
    {
        FAST,
        NORMAL,
        SLOW
    }

    public enum VIOLATION
    {
        NULL,
        CODING,
        DISREGARDING_TRAFFIC_SIGN_ZONE
    }

    public class GameManager : MonoBehaviour
    {
        #region Singleton
        public static GameManager instance;
        void Awake()
        {
            instance = this;
        }
        #endregion

        public GAME_STATE mState;
        public GAME_SPEED mSpeed;
        public GameData mData;


        void Start()
        {
            mState = GAME_STATE.PLAY;
            mSpeed = GAME_SPEED.NORMAL;
            Initialize();
        }

        void Update()
        {
            UpdateLevels();
            Cheats();
            
        }

        public bool GameIsPaused
        {
            get
            {
                if (mState == GAME_STATE.PAUSE)
                    return true;
                return false;
            }
        }

        #region Initialization
        void Initialize()
        {
            mData.Initialize();
        }

        public void UpdateLevels()
        {
            foreach(LevelData ldata in mData.LEVELS)
            {
                if(ldata.NextLevelUnlocked && ldata.nextLevel != null)
                {
                    ldata.nextLevel.Unlock();
                }
            }
        }

        public void AddHighScore(int levelIndex, int score)
        {
            var highScore = GetHighScore(levelIndex);

            bool levelStatFound = false;

            for (int i = 0; i < mData.LEVELS.Count; i++)
            {
                if (mData.LEVELS[i].LEVEL_NUMBER == levelIndex)
                {
                    if (mData.LEVELS[i].HIGH_SCORE < score)
                    {
                        mData.LEVELS[i].HIGH_SCORE = score;
                        levelStatFound = true;
                    }
                }

                if (levelStatFound)
                    return;
            }

            if(!levelStatFound)
                Debug.LogError("Level Data for level " + levelIndex + " not found.");
        }

        int GetHighScore(int levelIndex)
        {
            foreach (LevelData level in mData.LEVELS)
                if (level.LEVEL_NUMBER == levelIndex)
                    return level.HIGH_SCORE;

            return 0;
        }

        #endregion

        public void Unpause()
        {
            if (mState == GAME_STATE.PLAY)
                return;
            mState = GAME_STATE.PLAY;
        }

        public void Pause()
        {
            if (mState == GAME_STATE.PAUSE)
                return;
            mState = GAME_STATE.PAUSE;
        }

        public float GetMinimumToBreakRules(VEHICLE_STYLE _style)
        {
            for (int i = 0; i < mData.styleMinimums.Count; i++)
                if (mData.styleMinimums[i].style == _style)
                    return mData.styleMinimums[i].minimum;
            return -1;
        }

        #region Cheats

        void Cheats()
        {
            if (Input.GetKey(KeyCode.BackQuote) && Input.GetKey(KeyCode.Alpha1))
            {
                UnlockAllLevels();
                Debug.Log("Cheats: All levels unlocked");
            }

            if (Input.GetKey(KeyCode.BackQuote) && Input.GetKey(KeyCode.Alpha2))
            {
                AddPoints(1);
                Debug.Log("Cheats: 1 point added");
            }

            if (Input.GetKey(KeyCode.BackQuote) && Input.GetKey(KeyCode.Alpha3))
            {
                AddPoints(10);
                Debug.Log("Cheats: 10 points added");
            }

            if (Input.GetKey(KeyCode.BackQuote) && Input.GetKey(KeyCode.RightArrow))
            {
                SpeedUp();
                Debug.Log("Cheats: Time speed up");
            }

            if (Input.GetKey(KeyCode.BackQuote) && Input.GetKey(KeyCode.LeftArrow))
            {
                SlowDown();
                Debug.Log("Cheats: Time slowed down");
            }

            if (Input.GetKey(KeyCode.BackQuote) && Input.GetKey(KeyCode.DownArrow))
            {
                NormalizeTime();
                Debug.Log("Cheats: Time normalized");
            }
        }

        void UnlockAllLevels()
        {
            foreach (LevelData ld in mData.LEVELS)
                ld.Unlock();
        }

        void AddPoints(int points)
        {
            if (Player.instance != null)
                Player.instance.AddPoints(points);
        }

        public void SpeedUp()
        {
            mSpeed = GAME_SPEED.FAST;
            Time.timeScale = mData.speedUpMultiplier;

        }

        public void SlowDown()
        {
            mSpeed = GAME_SPEED.SLOW;
            Time.timeScale = mData.slowDownSpeed;
        }

        public void NormalizeTime()
        {
            mSpeed = GAME_SPEED.NORMAL;
            Time.timeScale = 1f;
        }

        #endregion
    }
}
