﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class CountdownManager : MonoBehaviour
    {
        #region Singleton
        public static CountdownManager instance;
        void Awake()
        {
            instance = this;
        }
        #endregion

        [System.Serializable]
        public struct Countdown
        {
            public float executeTime;
            public Violation violation;
        }


        public List<Countdown> queuedCountdowns = new List<Countdown>();

        public void QueueCountdown(Violation _violation)
        {
            Countdown cd = new Countdown();
            cd.executeTime = Time.time + _violation.data.apprehensionDuration;
            cd.violation = _violation;

            for (int i = 0; i <= queuedCountdowns.Count; i++)
            {
                if (queuedCountdowns.Count == 0)
                {
                    queuedCountdowns.Add(cd);
                    return;
                }

                if (cd.executeTime < queuedCountdowns[i].executeTime)
                {
                    queuedCountdowns.Insert(i, cd);
                    return;
                }
            }
        }

        void Update()
        {
            while(queuedCountdowns.Count > 0)
            {
                if (Time.time >= queuedCountdowns[0].executeTime)
                {
                    queuedCountdowns[0].violation.state = VIOLATION_STATE.TIMED_OUT;
                    queuedCountdowns.RemoveAt(0);
                }
                else
                    break;
            }
        }



    }
}
