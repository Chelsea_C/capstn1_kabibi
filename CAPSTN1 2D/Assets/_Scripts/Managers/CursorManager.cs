﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    #region Singleton
    public static CursorManager instance;
    void Awake()
    {
        instance = this;
    }
    #endregion

    enum STATE
    {
        DEFAULT,
        MOVE,
        TICKET
    }

    STATE state;

    [SerializeField]
    Texture2D moveCursor;
    [SerializeField]
    Texture2D ticketCursor;
    [SerializeField]
    Texture2D defaultCursor;

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
            SetMoveCursor();

        if (Input.GetMouseButtonUp(1))
        {
            state = STATE.DEFAULT;
            SetDefaultCursor();
        }
    }

    public void SetMoveCursor()
    {
        state = STATE.MOVE;
        Cursor.SetCursor(moveCursor, Vector2.zero, CursorMode.Auto);
    }

    public void SetTicketCursor()
    {
        if (state == STATE.MOVE)
            return;
        state = STATE.TICKET;
        Cursor.SetCursor(ticketCursor, Vector2.zero, CursorMode.Auto);
    }

    public void SetDefaultCursor()
    {
        if (state == STATE.MOVE)
            return;
        state = STATE.DEFAULT;
        Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
    }



}
