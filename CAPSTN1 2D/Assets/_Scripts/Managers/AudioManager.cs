﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class AudioManager : MonoBehaviour
    {
        #region Singleton
        public static AudioManager instance;
        void Awake()
        {
            instance = this;
        }
        #endregion

        
        public GameData data;

        public AudioSource source_click;
        public AudioSource source_correctTicket;
        public AudioSource source_incorrectTicket;
        public AudioSource source_bgm;
        public AudioSource source_ambience;
        public AudioSource source_levelEnd;
        
        int currentBgmIndex = 0;

        void Start()
        {
            PlayAmbience();
            currentBgmIndex = 0;
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
                PlayMouseClick();

            PlayBGMList();
        }

        void PlayAmbience()
        {
            if (source_ambience == null)
            {
                //Debug.LogError("No audio source for Ambience found.");
                return;
            }
            source_ambience.clip = data.BG_ambience;
            source_ambience.loop = true;
            source_ambience.Play();
        }

        public void PlayMouseClick()
        {
            if(source_click == null)
            {
                //Debug.LogError("No audio source for Click found.");
                return;
            }

            source_click.clip = data.mouseClick;
            source_click.Play();
        }

        public void PlayCorrectTicket()
        {
            if (source_correctTicket == null)
            {
                //Debug.LogError("No audio source for Correct Ticket found.");
                return;
            }
            source_correctTicket.clip = data.correctTicket;
            source_correctTicket.Play();
        }

        public void PlayIncorrectTicket()
        {
            if (source_incorrectTicket == null)
            {
                //Debug.LogError("No audio source for Incorrect Ticket found.");
                return;
            }
            source_incorrectTicket.clip = data.incorrectTicket;
            source_incorrectTicket.Play();
        }

        public void PlayLevelEnd()
        {
            if (source_levelEnd == null)
            {
                //Debug.LogError("No audio source for LevelEnd found.");
                return;
            }


            StartFade(source_bgm, 1f, 0);

            source_levelEnd.clip = data.levelEnd;
            source_levelEnd.Play();
        }

        void PlayBGMList()
        {
            if (source_bgm == null)
            {
                //Debug.LogError("No audio source for BGM found.");
                return;
            }
            if (data.BGM_List[currentBgmIndex] != null && !source_bgm.isPlaying)
            {
                source_bgm.clip = data.BGM_List[currentBgmIndex];
                source_bgm.Play();
                if(currentBgmIndex + 1 == data.BGM_List.Count)
                    currentBgmIndex = 0;
            }
        }

        public static IEnumerator StartFade(AudioSource audioSource, float duration, float targetVolume)
        {
            float currentTime = 0;
            float start = audioSource.volume;

            while (currentTime < duration)
            {
                currentTime += Time.deltaTime;
                audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
                yield return null;
            }
            yield break;
        }
    }

}