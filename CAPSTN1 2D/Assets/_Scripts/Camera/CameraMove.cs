﻿using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public float panSpeed = 10.0f;
    public bool allowEdgeScrolling = false;
    public float edgeThickness = 10.0f;
    public bool allowMouseDrag = true;
    public float dragSpeed = 30.0f;
    public bool AllowMoveWhenPaused = false;

    public float calculatedMaxXOrtho;
    public float calculatedMaxYOrtho;

    float minX;
    float maxX;
    float maxY;
    float minY;
    Transform target;

    public SpriteRenderer spriteBounds;
    public Collider2D boundary;

    void Start ()
    {
        float vertExtent = Camera.main.GetComponent<Camera>().orthographicSize;
        float horzExtent = vertExtent * Screen.width / Screen.height;

        boundary = spriteBounds.GetComponent<Collider2D>();

        minX = (float)(horzExtent - spriteBounds.sprite.bounds.size.x / 2.0f);
        maxX = (float)(spriteBounds.sprite.bounds.size.x / 2.0f - horzExtent);
        minY = (float)(vertExtent - spriteBounds.sprite.bounds.size.y / 2.0f);
        maxY = (float)(spriteBounds.sprite.bounds.size.y / 2.0f - vertExtent);

        calculatedMaxXOrtho = boundary.bounds.size.x / 2;
        calculatedMaxYOrtho = boundary.bounds.size.y / 2;
    }

    void FixedUpdate()
    {
        if (!AllowMoveWhenPaused && Urbana.GameManager.instance.GameIsPaused)
            return;

        allowEdgeScrolling = Urbana.GameManager.instance.mData.AllowEdgeScrolling;

        Vector3 pos = transform.position;
        
        #region Edge scrolling
        if (allowEdgeScrolling)
        {
            if (Input.mousePosition.y >= Screen.height - edgeThickness)
            {
                pos.y += panSpeed * Time.deltaTime;
            }

            if (Input.mousePosition.y <= edgeThickness)
            {
                pos.y -= panSpeed * Time.deltaTime;
            }

            if (Input.mousePosition.x >= Screen.height - edgeThickness)
            {
                pos.x += panSpeed * Time.deltaTime;
            }

            if (Input.mousePosition.x <= edgeThickness)
            {
                pos.x -= panSpeed * Time.deltaTime;
            }
        }
        #endregion

        #region Movement keys (WASD and Arrows)
        if (Input.GetKey("w") || Input.GetKey("up"))
        {
            pos.y += panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("s") || Input.GetKey("down"))
        {
            pos.y -= panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("d") || Input.GetKey("right"))
        {
            pos.x += panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("a") || Input.GetKey("left"))
        {
            pos.x -= panSpeed * Time.deltaTime;
        }
        #endregion 

        #region Mouse dragging 
        if (Input.GetMouseButton(1))
        {
            if (Input.GetAxis("Mouse X") > 0)
            {
                pos -= new Vector3(Input.GetAxisRaw("Mouse X") * Time.deltaTime * dragSpeed,
                                           Input.GetAxisRaw("Mouse Y") * Time.deltaTime * dragSpeed, 0.0f);
            }

            else if (Input.GetAxis("Mouse X") < 0)
            {
                pos -= new Vector3(Input.GetAxisRaw("Mouse X") * Time.deltaTime * 2 * dragSpeed,
                                           Input.GetAxisRaw("Mouse Y") * Time.deltaTime * dragSpeed, 0.0f);
            }
        }
        #endregion
        
        transform.position = pos;
        Clamp();        
    }


    void Clamp()
    {
        // In case camera bounds are taller than map bounds
        if(Camera.main.orthographicSize * 2 > boundary.bounds.size.y)
        {
            Camera.main.orthographicSize = boundary.bounds.size.y / 2;
            GetComponent<CameraZoom>().maxOrtho = Camera.main.orthographicSize;
            GetComponent<CameraZoom>().targetOrtho = Camera.main.orthographicSize;
        }

        float screenAspect = (float)Screen.width / (float)Screen.height;
		float camHalfHeight = Camera.main.orthographicSize;
		float camHalfWidth = screenAspect * camHalfHeight;
        
        // In case camera bounds are wider than map bounds
        if(camHalfWidth > boundary.bounds.size.x / 2)
        {
            camHalfWidth = boundary.bounds.size.x / 2;
            //GetComponent<CameraZoom>().maxOrtho = Camera.main.orthographicSize;
            GetComponent<CameraZoom>().maxOrtho = Camera.main.orthographicSize;
            GetComponent<CameraZoom>().targetOrtho = Camera.main.orthographicSize;
        }

		float xMax = boundary.bounds.max.x - camHalfWidth;
		float xMin = boundary.bounds.min.x + camHalfWidth;
		float yMax = boundary.bounds.max.y - camHalfHeight;
		float yMin = boundary.bounds.min.y + camHalfHeight;
		float z = transform.position.z;

		transform.position = new Vector3 (
			Mathf.Clamp (transform.position.x, xMin, xMax),
			Mathf.Clamp (transform.position.y, yMin, yMax),
			z);
    }

    //void LateUpdate()
    //{
    //    Vector3 pos = transform.position;

    //    #region Bounds
    //    pos.x = Mathf.Clamp(pos.x, minX, maxX);
    //    pos.y = Mathf.Clamp(pos.y, minY, maxY);
    //    #endregion

    //    transform.position = pos;
    //}
}
