﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom: MonoBehaviour
{
    public float zoomSpeed = 1;
    public float targetOrtho;
    public float smoothSpeed = 2.0f;
    public float minOrtho = 1.0f;
    public float maxOrtho = 20.0f;
    public bool AllowZoomWhenPaused = false;

    [Header("License Plates")]
    public LayerMask licensePlatesLayer;
    public LayerMask everything;
    public bool AllowHidingLicensePlates = false;
    public float licensePlateMaxOrtho = 20f;
    
    void Start()
    {
        targetOrtho = Camera.main.orthographicSize;
    }

    void Update()
    {
        if (!AllowZoomWhenPaused && Urbana.GameManager.instance.GameIsPaused)
            return;

        if (AllowHidingLicensePlates && targetOrtho >= licensePlateMaxOrtho)
            HideLicensePlates();
        else
            UnhideLicensePlates();


        if (GetComponent<CameraMove>().calculatedMaxXOrtho < GetComponent<CameraMove>().calculatedMaxYOrtho)
            maxOrtho = GetComponent<CameraMove>().calculatedMaxXOrtho;
        else
            maxOrtho = GetComponent<CameraMove>().calculatedMaxYOrtho;

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0.0f)
        {
            targetOrtho -= scroll * zoomSpeed;
            targetOrtho = Mathf.Clamp(targetOrtho, minOrtho, maxOrtho);
        }

        Camera.main.orthographicSize = Mathf.MoveTowards(Camera.main.orthographicSize, targetOrtho, smoothSpeed * Time.deltaTime);
    }

    void HideLicensePlates()
    {
        Camera.main.cullingMask = licensePlatesLayer;
    }

    void UnhideLicensePlates()
    {
        Camera.main.cullingMask = everything;
    }
}
