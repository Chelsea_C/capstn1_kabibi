﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class Obstacle : MonoBehaviour
    {
        public List<Violation> issuedViolations;

        void Start()
        {
            gameObject.tag = "Obstacle";
            gameObject.layer = 9;
        }

    }
}