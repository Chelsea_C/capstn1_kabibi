﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class SpaceChecker : MonoBehaviour
    {
        public bool isOccupied = false;

        [ReadOnly]
        public Vehicle currentOccupant;

        void Start()
        {
            gameObject.tag = "Checker";
        }

        public void ClearVehicle()
        {
            isOccupied = false;
            currentOccupant = null;
        }

        void OnTriggerStay(Collider other)
        {
            if(other.tag == "Vehicle")
            {

            }
                
        }
    }
}
