﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class Intersection : MonoBehaviour
    {

        public List<Stoplight> stoplights;
        public Stoplight activeLight;
        public float timeBetweenSwitch = 5f;
        public List<Vehicle> vehicleQueue;
        public ViolationData obstructionViolation;
        public ViolationData disregardingSignsViolation;

        [ReadOnly]
        public int stoplightIndex = 0;

        void Start()
        {
            stoplights = new List<Stoplight>();
            var init_stoplights = GetComponentsInChildren<Stoplight>();
            foreach (Stoplight sl in init_stoplights)
                stoplights.Add(sl);

            StartCoroutine(AutomateLights());

            if (obstructionViolation == null)
                obstructionViolation = GameManager.instance.mData.obstruction;
            if (disregardingSignsViolation == null)
                disregardingSignsViolation = GameManager.instance.mData.disregardingSigns;
        }

        
        IEnumerator AutomateLights()
        {            
            while (true)
            {
                if (stoplightIndex >= stoplights.Count)
                    stoplightIndex = 0;

                ActivateLight();
                yield return new WaitForSeconds(timeBetweenSwitch);
                if (GameManager.instance.mState == GAME_STATE.PLAY)
                    stoplightIndex++;
            }

            
        }

        void ActivateLight()
        {
            foreach (Stoplight sl in stoplights)
                if (sl == stoplights[stoplightIndex])
                {
                    sl.state = LIGHT_STATE.GO;
                    activeLight = sl;
                }
                else
                    sl.state = LIGHT_STATE.STOP;
        }

        void RemoveFromQueue(Vehicle vehicle)
        {
            int index = 0;
            for (int i = 0; i < vehicleQueue.Count; i++)
                if (vehicle == vehicleQueue[i])
                    index = i;

            vehicleQueue.RemoveAt(index);
        }
        
        void AddToQueue(Vehicle vehicle)
        {
            vehicleQueue.Add(vehicle);
        }

        public int GetPriorityInQueue(Vehicle vehicle)
        {
            if (vehicleQueue.Count == 0)
                return 0;

            for (int i = 0; i < vehicleQueue.Count; i++)
                if (vehicle == vehicleQueue[i])
                    return i;

            return -1;
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Vehicle")
            {
                var vehicle = other.GetComponent<Vehicle>();
                vehicle.IsInIntersection = true;
                vehicle.currentIntersection = this;

                AddToQueue(vehicle);
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.tag == "Vehicle")
            {
                var vehicle = other.GetComponent<Vehicle>();
                vehicle.IsInIntersection = false;
                vehicle.currentIntersection = null;

                RemoveFromQueue(vehicle);
            }
        }


    }
}