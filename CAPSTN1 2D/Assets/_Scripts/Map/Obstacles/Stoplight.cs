﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public enum LIGHT_STATE
    {
        STOP,
        SLOW,
        GO
    }

    public class Stoplight : Obstacle
    {
        public LIGHT_STATE state;
        public Color goColor = Color.green;
        public Color slowColor = Color.yellow; // Necessary?
        public Color stopColor = Color.red;

        SpriteRenderer sprite;
        public ViolationData violation;

        void Start()
        {
            sprite = GetComponent<SpriteRenderer>();
            goColor.a = 1;
            slowColor.a = 1;
            stopColor.a = 1;
            gameObject.layer = 9;
        }

        void Update()
        {
            Light();
        }

        public void SetTo(LIGHT_STATE _state)
        {
            state = _state;
        }

        void Light()
        {
            switch (state)
            {
                case LIGHT_STATE.GO:
                    sprite.color = goColor;
                    break;
                case LIGHT_STATE.SLOW:
                    sprite.color = slowColor;
                    break;
                case LIGHT_STATE.STOP:
                    sprite.color = stopColor;
                    break;
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if(state != LIGHT_STATE.STOP)
                return;

            Debug.Log("Trigger");
            if(other.tag == "Vehicle")
            {
                Debug.Log("Ticketing RED LIGHT");
                LevelManager.instance.SetViolation(this, other.GetComponent<Vehicle>(), "Disregarding Traffic Signs");
            }
        }
    }
}
