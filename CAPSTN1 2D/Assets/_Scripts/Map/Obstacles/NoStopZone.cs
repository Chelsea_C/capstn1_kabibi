﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class NoStopZone : Obstacle
    {
        public bool hasForwardRoom;
        public bool isObstructed;
        public SpaceChecker forwardChecker;
        public VehicleChecker vehicleChecker;
        //public Vehicle currentOccupant;
        public ViolationData violation;

        [ReadOnly]
        public float timer;
        [SerializeField][ReadOnly]
        bool trigger = false;

        void Start()
        {
            if(forwardChecker == null)
                forwardChecker = GetComponentInChildren<SpaceChecker>();

            if (forwardChecker == null)
                Debug.LogError("No checker attached");

            if (vehicleChecker == null)
                vehicleChecker = GetComponentInChildren<VehicleChecker>();

            if (vehicleChecker == null)
                Debug.LogError("No checker attached");
        }

        void Update()
        {
            hasForwardRoom = !forwardChecker.isOccupied;
            isObstructed = vehicleChecker.isOccupied;

            if (!isObstructed)
            //if (currentOccupant == null)
            {
                timer = 0;
                trigger = false;
                return;
            }

            timer += Time.deltaTime;
            if (timer >= violation.violationDuration && !trigger)// && currentOccupant.GetComponent<Vehicle>().state == VEHICLE_STATE.WAIT)
            //if (currentOccupant.GetComponent<Vehicle>().state == VEHICLE_STATE.WAIT && !trigger)
            {
                trigger = true;
                Debug.Log("Issuing violation of " + violation.name + " to " + vehicleChecker.occupant.name);
                LevelManager.instance.SetViolation(this, vehicleChecker.occupant, violation.name);
            }
        }

        /*
        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Vehicle")
            {

                currentOccupant = other.GetComponent<Vehicle>();
            }
        }*/



        void OnTriggerExit(Collider other)
        {
            if (other.tag == "Vehicle")
            {
                if (trigger)
                    foreach (Violation vio in other.GetComponent<Vehicle>().violations)
                        foreach (Violation aVio in issuedViolations)
                            if (aVio == vio)
                            {
                                Debug.Log("Violation countdown queued");
                                CountdownManager.instance.QueueCountdown(aVio);
                                issuedViolations.Remove(aVio);
                                trigger = false;
                            }
                vehicleChecker.occupant = null;
            }
            
        }
    }
}
