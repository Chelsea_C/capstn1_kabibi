﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class VehicleChecker : MonoBehaviour
    {
        public LayerMask vehicles;
        public Vehicle occupant;
        public bool isOccupied;
        public float detectionRadius;

        public List<Vehicle> detectedVehicles = new List<Vehicle>();

        void Update()
        {
            CheckForVehicles();

            //if (detectedVehicles.Count > 0)
            //    occupant = detectedVehicles[0];
            //else
            //    occupant = null;

            if (occupant != null)
                isOccupied = true;
            else
                isOccupied = false;
        }

        void CheckForVehicles()
        {
            //Use the OverlapBox to detect if there are any other colliders within this box area.
            //Use the GameObject's centre, half the size (as a radius) and rotation. This creates an invisible box around your GameObject.
            //Collider[] hitColliders = Physics.OverlapBox(gameObject.transform.position, transform.lossyScale / 2, Quaternion.identity, vehicles);
            Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, detectionRadius, vehicles);

            UpdateVehiclesDetected(hitColliders);
            
            if (hitColliders.Length <= 0)
            {
                occupant = null;
                return;
            }


            
            //int i = 0;
            ////Check when there is a new collider coming into contact with the box
            //while (i < hitColliders.Length)
            //{
            //    //Output all of the collider names
            //    Debug.Log("Hit : " + hitColliders[i].name + i);
            //    //Increase the number of Colliders in the array
            //    i++;
            //}
        }

        void UpdateVehiclesDetected(Collider[] colliders)
        {
            detectedVehicles = new List<Vehicle>();
            for(int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i] != this.gameObject.transform.GetComponent<Collider>())
                    detectedVehicles.Add(colliders[i].GetComponent<Vehicle>());
            }

            if(detectedVehicles.Count > 0)
                occupant = detectedVehicles[0];
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            //Check that it is being run in Play Mode, so it doesn't try to draw this in Editor mode
            //Draw a cube where the OverlapBox is (positioned where your GameObject is as well as a size)
            //Gizmos.DrawWireCube(transform.position, transform.lossyScale);
            Gizmos.DrawWireSphere(this.transform.position, detectionRadius);
        }
    }
}