﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetResolution : MonoBehaviour
{
    public bool forceFullScreen = false;

    void Start()
    {
        Screen.SetResolution(1024, 768, Screen.fullScreenMode);
    }
}
