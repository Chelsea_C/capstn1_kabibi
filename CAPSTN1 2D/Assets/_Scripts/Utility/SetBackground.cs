﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBackground : MonoBehaviour
{
    void Awake()
    {
        Camera.main.GetComponent<CameraMove>().spriteBounds = this.GetComponent<SpriteRenderer>();
    }
}
