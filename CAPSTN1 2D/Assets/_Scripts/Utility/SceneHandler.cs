﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    public static SceneHandler instance;
    void Awake()
    {
        instance = this;
    }

    [Header("Screen Fade")]
    public Color loadToColor = Color.black;
    public float loadSpeed = 1.0f;

    public void GoToScene(string name)
    {
        SceneManager.LoadScene(name, LoadSceneMode.Single);
    }

    public void GoToScene(int index)
    {
        SceneManager.LoadScene(index, LoadSceneMode.Single);
    }

    public void GoToSceneWithFade(int index)
    {
        var scene = SceneManager.GetSceneByBuildIndex(index);
        GoToSceneWithFade(scene.name);
    }

    public void GoToSceneWithFade(string name)
    {
        Initiate.Fade(name, loadToColor, loadSpeed);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ReloadSceneWithFade()
    {
        GoToSceneWithFade(SceneManager.GetActiveScene().name);
    }
}
