﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewableArea : MonoBehaviour
{
    public static ViewableArea instance;
    void Awake()
    {
        instance = this;
    }

    public float Height { get { return GetComponent<SpriteRenderer>().bounds.size.y; } }
    public float Width { get { return GetComponent<SpriteRenderer>().bounds.size.x; } }

    public float h;
    public float w;

    void Update()
    {
        h = Height;
        w = Width;
    }

}
