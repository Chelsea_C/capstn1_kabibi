﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelStats : MonoBehaviour
{
    public int number;
    public int highestScore;
}
