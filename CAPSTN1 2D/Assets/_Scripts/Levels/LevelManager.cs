﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace Urbana
{
    [System.Serializable]
    public class ColorCodingScheme
    {
        public int number;
        public Color color;
    }

    [System.Serializable]
    public class CodingScheme
    {
        public System.DayOfWeek day;
        public List<int> bannedNumbers;
    }

    public class LevelManager : MonoBehaviour
    {

        #region Singleton
        public static LevelManager instance;
        void Awake()
        {
            instance = this;
        }
        #endregion

        [Header("Screens")]
        [ReadOnly] public Canvas levelStart;
        [ReadOnly] public Canvas levelEnd;
        [ReadOnly] public Canvas pauseScreen;
        [ReadOnly] public Canvas hud;

        [Header("Tools Menu")]
        public float delayBetweenButtonSpawn = 0.06f;
        public Action deleter;

        [Header("Level Info")]
        [ReadOnly] public int currentLevelNumber;
        [ReadOnly] public Level currentLevel;
        [ReadOnly] public int score;

        [Header("Day")]
        public DayOfWeek currentDay;
        public int currentDayIndex;
        public float timeUntilNextDay;

        public GameObject vehiclePrefab;

        [Header("Timer")]
        [ReadOnly] public Image sprite_Timer;
        [SerializeField] [ReadOnly]
        long currCountdownValue;
        long countdownValue = 120;
        public bool IsTimerActive = false;
        public string TIMER_DISPLAY_TEXT;


        [Header("Vehicles, Sprites and Colors")]
        public List<Vehicle> vehicles = new List<Vehicle>();
        public float timeBetweenSpawns;
        public int maxCount;
        public int minCount;
        public List<Sprite> licenseSprites;
        public List<Color> colors;

        [Header("Violation Prefabs")]
        public List<GameObject> violationPrefabs;

        public bool HasStarted;
        public bool HasEnded;

        void Start()
        {
            HasStarted = false;
            HasEnded = false;
            // Level will initialize itself to LevelManager
        }

        void Update()
        {
            if (Input.anyKeyDown && !IsTimerActive)
            {
                levelStart.gameObject.SetActive(false);
                StartLevel();
                HasStarted = true;
            }

            if (HasEnded)
                return;

            TrackTime();
            TrackViolations();

            score = Player.instance._points;
        }

        #region Level Management
        public void PauseLevel()
        {
            if (!HasStarted || HasEnded)
                return;
            GameManager.instance.Pause();
            UnhideCanvas(pauseScreen);
            pauseScreen.GetComponent<PauseScreen>().OnPause();
        }

        public void UnpauseLevel()
        {
            if (!HasStarted || HasEnded)
                return;
            GameManager.instance.Unpause();
            pauseScreen.GetComponent<PauseScreen>().OnResume();
            //HideCanvas(pauseScreen);
            
        }

        public void Initialize(Level level)
        {
            score = 0;
            currentLevel = level;
            currentLevelNumber = level.data.LEVEL_NUMBER;
            timeBetweenSpawns = level.timeBetweenSpawns;
            maxCount = level.maxCount;
            minCount = level.minCount;

            SetTimer(level.timeLimit);
            SetDay(level.startingDay);

            HideCanvas(levelEnd);
            HideCanvas(hud);
        }

        void StartLevel()
        {
            GameManager.instance.Unpause();
            GameManager.instance.NormalizeTime();

            UnhideCanvas(hud);

            StartTimer();
            StartCoroutine(SpawnVehicles());
        }

        void EndLevel()
        {
            Debug.Log("Ending level");
            StopAllCoroutines();
            HasEnded = true;

            //GameManager.instance.Pause();
            sprite_Timer.enabled = false;

            if (score > currentLevel.data.HIGH_SCORE)
            {
                currentLevel.data.SetHighScore = score;
            }

            HideCanvas(hud);

            GameManager.instance.NormalizeTime();
            AudioManager.instance.PlayLevelEnd();

            levelEnd.GetComponent<EndScreen>().ManualUpdate();
            UnhideCanvas(levelEnd);
        }

        void HideCanvas(Canvas canvas)
        {
            if(canvas == null)
            {
                return;
            }
            canvas.gameObject.SetActive(false);
        }

        void UnhideCanvas(Canvas canvas)
        {
            if (canvas == null)
            {
                return;
            }
            canvas.gameObject.SetActive(true);
        }

        #endregion

        #region Spawning Vehicles

        IEnumerator SpawnVehicles()
        {
            while (true)
            {
                yield return new WaitForSeconds(timeBetweenSpawns);
                if (vehicles.Count < minCount && !GameManager.instance.GameIsPaused)
                    SpawnAVehicle();
            }
        }

        void SpawnAVehicle()
        {
            if (currentLevel.data.STYLES.Count < 0)
            {
                Debug.LogError("Vehicle styles not specified for Level " + currentLevelNumber);
                return;
            }

            /// Get Random spawn and destination points
            WayPoint start;
            WayPoint end;
            do
            {
                start = currentLevel.starting_waypoints[UnityEngine.Random.Range(0, currentLevel.starting_waypoints.Count)];
                end = currentLevel.ending_waypoints[UnityEngine.Random.Range(0, currentLevel.ending_waypoints.Count)];
            } while (start.clusterNumber != end.clusterNumber);


            /// Instantiate object
            GameObject obj = Instantiate(vehiclePrefab, start.transform);
            obj.transform.parent = GameObject.Find("Cars").transform;
            obj.transform.position = start.transform.position;

            obj.GetComponent<SpriteRenderer>().sortingOrder = start.clusterSortingOrder;

            /// Set start and end points for path
            FindPath path = obj.GetComponent<FindPath>();
            path.SetStartAndEndPoints(start, end);
            
            /// Set vehicle speeds and state to moving
            Vehicle vehicle = obj.GetComponent<Vehicle>();
            if (start.isOnHighway)
                vehicle.maxSpeed = (float)currentLevel.maxSpeed;
            else
                vehicle.maxSpeed = (float)currentLevel.minSpeed;
            //vehicle.maxSpeed = (float)(UnityEngine.Random.Range(currentLevel.minSpeed, currentLevel.maxSpeed));
            vehicle.timeUntilDestroyed = currentLevel.timeUntilDespawn;

            ///Randomize VEHICLE_STYLE and check if it is enabled for the level
            do
            {
                vehicle.style = RandomizeVehicleStyle();
                
            } while (!currentLevel.data.STYLES.Contains(vehicle.style));
            //Debug.Log(vehicle.style);

            // Set any forced decisions
            if (vehicle.style != VEHICLE_STYLE.SAINT)
            {
                vehicle.AlwaysFollowLights = currentLevel.data.AlwaysFollowLights;
                vehicle.AlwaysFollowNoStopZone = currentLevel.data.AlwaysFollowNoStopZone;
            }

            /// Randomize License plate ending
            vehicle.licenseEnding = UnityEngine.Random.Range(0, 10);
            vehicle.licensePlate.sprite = licenseSprites[vehicle.licenseEnding];

            /// Assign Color according to license ending                        
            SpriteRenderer sprite = obj.GetComponent<SpriteRenderer>();
            sprite.color = GetColor(vehicle.licenseEnding);
            //if (vehicle.style == VEHICLE_STYLE.SAINT)
            //    sprite.color = Color.white;
            //if (vehicle.style == VEHICLE_STYLE.EVIL)
            //    sprite.color = Color.black;
            
            vehicles.Add(vehicle);
            CheckCoding(vehicle);

            /// Randomize speed
            //vehicle.maxSpeed = UnityEngine.Random.Range(currentLevel.minSpeed, currentLevel.maxSpeed);
            //vehicle.SetSpeed(UnityEngine.Random.Range(currentLevel.minSpeed, currentLevel.maxSpeed));
        }
        
        Color RandomizeColor()
        {
            int index = UnityEngine.Random.Range(0, colors.Count);
            return colors[index];
        }

        public Color GetColor(int index)
        {
            return colors[index];
        }

        VEHICLE_STYLE RandomizeVehicleStyle()
        {
            int index = UnityEngine.Random.Range(0, Enum.GetNames(typeof(VEHICLE_STYLE)).Length);
            return (VEHICLE_STYLE)index;
        }

        #endregion

        #region Day

        void SetDay(int index)
        {
            currentDayIndex = index;
            currentDay = (DayOfWeek)index;
        }

        string RandomizeStartingDay()
        {
            currentDayIndex = UnityEngine.Random.Range(0, 7);
            currentDay = (DayOfWeek)currentDayIndex;
            return GetDayName(currentDayIndex);
        }

        string GetDayName(int index)
        {
            string name = " ";
            if (index > 6 || index < 0)
            {
                Debug.Log("Error retrieving day");
                return name;
            }

            switch (index)
            {
                case (int)DayOfWeek.Sunday:
                    name = "Sunday";
                    break;
                case (int)DayOfWeek.Monday:
                    name = "Monday";
                    break;
                case (int)DayOfWeek.Tuesday:
                    name = "Tuesday";
                    break;
                case (int)DayOfWeek.Wednesday:
                    name = "Wednesday";
                    break;
                case (int)DayOfWeek.Thursday:
                    name = "Thursday";
                    break;
                case (int)DayOfWeek.Friday:
                    name = "Friday";
                    break;
                case (int)DayOfWeek.Saturday:
                    name = "Saturday";
                    break;
            }
            currentDayIndex = index;
            return name;
        }

        void GoToNextDay()
        {
            currentDayIndex++;
        }

        #endregion

        #region TimeOfDay
        void RandomizeStartingTime()
        {

        }
        #endregion

        #region Timer
        void TrackTime()
        {
            if (currCountdownValue > 0)
                sprite_Timer.fillAmount = 1 - (float)(countdownValue - currCountdownValue) / countdownValue;
            
        }

        void StartTimer()
        {
            IsTimerActive = true;
            //sprite_Timer.DOColor(Color.red, countdownValue).SetEase(Ease.Linear);
            StartCoroutine(StartCountdown());
        }

        void StopAndResetTimer()
        {
            StopAllCoroutines();
            currCountdownValue = countdownValue;
            IsTimerActive = false;
        }

        public void SetTimer(float seconds)
        {
            sprite_Timer.fillAmount = 1;
            countdownValue = (long)seconds;
            currCountdownValue = countdownValue;
        }

        IEnumerator StartCountdown()
        {
            currCountdownValue = countdownValue;
            while (currCountdownValue > 0)
            {
                yield return new WaitForSeconds(1f);
                if (GameManager.instance.mState == GAME_STATE.PLAY)
                    currCountdownValue--;
                if (currCountdownValue == 0)
                    EndLevel();
            }
        }

        #endregion

        #region Car Violations

        void CheckCoding(Vehicle _vehicle)
        {
            if (currentDay == DayOfWeek.Sunday || currentDay == DayOfWeek.Saturday)
                return;

            List<int> banned = new List<int>(GetBannedNumbers(currentDay));
            if (banned != null)
            {
                if (banned.Contains(_vehicle.licenseEnding))
                    SetCodingViolation(_vehicle);
            }
        }

        public List<int> GetBannedNumbers(DayOfWeek day)
        {
            foreach (CodingScheme cs in GameManager.instance.mData.colorCoding)
                if (cs.day == day)
                    return cs.bannedNumbers;
            return null;
        }

        void SetCodingViolation(Vehicle _vehicle)
        {
            for (int i = 0; i < violationPrefabs.Count; i++)
            {
                if (violationPrefabs[i].GetComponent<Violation>().data.name == "Coding")
                {
                    GameObject obj = Instantiate(violationPrefabs[i], _vehicle.transform);
                    obj.transform.parent = _vehicle.transform;
                    break;
                }
            }
        }

        public void SetViolation(Obstacle _obstacle, Vehicle _vehicle, string _violation)
        {
            if (IsViolationEnabled(_violation))
            {

               
                var violation = GetViolation(_violation);
                GameObject obj = Instantiate(violation, _vehicle.transform);
                obj.transform.parent = _vehicle.transform;

                Violation vObj = obj.GetComponent<Violation>();
                _obstacle.issuedViolations.Add(vObj);
                Debug.Log("Issued violation of " + violation.name + " to " + _vehicle.name);
            }
        }

        public GameObject GetViolation(string violationName)
        {
            foreach (GameObject obj in violationPrefabs)
            {
                if (obj.GetComponent<Violation>().data.name == violationName)
                    return obj;
            }
            Debug.LogError("Violation " + violationName + " not found for Level");
            return null;
        }

        public void RemoveViolation(Vehicle _vehicle, string _violation)
        {

        }

        void TrackViolations()
        {
            foreach (Vehicle v in vehicles)
            {

            }
        }

        bool IsViolationEnabled(string violationName)
        {
            foreach (ViolationData vd in currentLevel.data.VIOLATIONS_ENABLED)
                if (violationName == vd.name)
                    return true;

            return false;
        }

        #endregion

        #region UI

        public void SpawnToolMenu()
        {
            if (HasEnded || pauseScreen.gameObject.activeSelf)
                return;

            GameManager.instance.Pause();
            RadialMenuManager.instance.SpawnMenu(currentLevel.tools, delayBetweenButtonSpawn);
        }

        public void HideInstructionPanels()
        {
            foreach (GameObject go in currentLevel.panels)
                go.SetActive(false);
        }

        public void ShowInstructionPanels()
        {
            foreach (GameObject go in currentLevel.panels)
                go.SetActive(true);
        }


        #endregion
    }
}
