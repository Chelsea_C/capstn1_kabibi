﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{

    public class Level : MonoBehaviour
    {
        public static Level instance;
        void Awake()
        {
            instance = this;
        }

        public LevelData data;
        [HideInInspector]
        public float timeLimit;
        [ReadOnly]
        public int startingDay = 0;

        [Header("Vehicles")]
        public int maxCount;
        public int minCount;
        public float timeBetweenSpawns = 3f;
        public int minSpeed;
        public int maxSpeed;
        public float timeUntilDespawn = 3f;

        [Header("Waypoints")]
        [ReadOnly]
        public List<WayPoint> waypoints;
        [ReadOnly]
        public List<WayPoint> starting_waypoints;
        [ReadOnly]
        public List<WayPoint> ending_waypoints;
        
        [Header("Rules and Violations")]
        public List<GameObject> violationPrefabs;

        [Header("Tools Enabled")]
        [ReadOnly]
        public Action[] tools;

        [Header("Instruction Panels")]
        public List<GameObject> panels;


        void Start()
        {
            timeLimit = data.TIME_LIMIT;
            GenerateWaypointsList();
            InitializeTools();
            SetStartDay();
            LevelManager.instance.Initialize(this);
        }

        void GenerateWaypointsList()
        {
            var wps = GetComponentsInChildren<WayPoint>();
            foreach (WayPoint w in wps)
                waypoints.Add(w);

            foreach (WayPoint w in wps)
                if (w.type == WAYPOINT_TYPE.END)
                    ending_waypoints.Add(w);

            foreach (WayPoint w in wps)
                if (w.type == WAYPOINT_TYPE.START)
                    starting_waypoints.Add(w);
        }

        void InitializeTools()
        {
            List<Action> actionList = new List<Action>();

            if (data.VIOLATIONS_ENABLED.Count <= 0)
            {
                Debug.LogError("No violations found enabled for Level " + data.LEVEL_NUMBER);
                return;
            }

            foreach(ViolationData vd in data.VIOLATIONS_ENABLED)
            {
                Action action = new Action();
                action.sprite = vd.icon;
                action.color = vd.color;
                action.title = vd.name;
                actionList.Add(action);
            }

            foreach(ToolData td in data.TOOLS_ENABLED)
            {
                Action action = new Action();
                action.sprite = td.icon;
                action.color = td.color;
                action.title = td.name;
                actionList.Add(action);
            }

            tools = actionList.ToArray();
        }

        void SetStartDay()
        {
            do
            {
                startingDay = UnityEngine.Random.Range(0, 7);
            } while (!data.allowedDays.Contains((System.DayOfWeek)startingDay));
        }    
    }
}