﻿using System.Collections;
using UnityEngine;

namespace Urbana
{
    public enum FLOATING_TYPE
    {
        POINTS,
        CURSE
    }

    public class FloatingTextManager : MonoBehaviour
    {
        #region Singleton
        public static FloatingTextManager instance;
        void Awake()
        {
            instance = this;
        }
        #endregion
        
        public GameObject popupTextPrefab_curse;
        public GameObject popupTextPrefab_score;
        public GameObject floatingTextCanvas;

        public void CreateFloatingText(string text, Transform location, FLOATING_TYPE type)
        {
            if (type == FLOATING_TYPE.POINTS)
            {
                GameObject obj = Instantiate(popupTextPrefab_score);
                FloatingText instance = obj.GetComponent<FloatingText>();
                Vector2 screenPosition = Camera.main.WorldToScreenPoint(location.position);
                instance.transform.SetParent(floatingTextCanvas.transform, false);
                instance.transform.position = screenPosition;
                instance.SetText(text);
            }

            if (type == FLOATING_TYPE.CURSE)
            {
                GameObject obj = Instantiate(popupTextPrefab_curse);
                FloatingText instance = obj.GetComponent<FloatingText>();
                Vector2 screenPosition = Camera.main.WorldToScreenPoint(location.position);
                instance.transform.SetParent(floatingTextCanvas.transform, false);
                instance.transform.position = screenPosition;
                instance.SetText(text);
            }
        }
    }
}