﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    public class FloatingText : MonoBehaviour
    {
        public Animator animator;
        public Text mText;

        void Start()
        {
            if (animator == null)
                animator = GetComponentInChildren<Animator>();

            AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
            Destroy(gameObject, clipInfo[0].clip.length);

            if (mText == null)
                mText = GetComponentInChildren<Text>();
        }

        public void SetText(string text)
        {
            mText.text = text;
        }
    }
}