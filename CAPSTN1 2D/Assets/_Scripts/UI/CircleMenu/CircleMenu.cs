﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class MenuButton
{
    public string name;
    public Image sceneImage;
    public Color normalColor = Color.white;
    public Color highlightColor = Color.grey;
    public Color pressedColor = Color.grey;
}

public class CircleMenu : MonoBehaviour
{

    public List<MenuButton> buttons = new List<MenuButton>();
    Vector2 mousePosition;
    Vector2 fromVector2M = new Vector2(0.5f, 1.0f);
    Vector2 centerCircle = new Vector2(0.5f, 0.5f);
    Vector2 toVector2M;

    public int menuItemCount;
    public int currentMenuItemIndex;
    int previousMenuItemIndex;

    void Start()
    {
        menuItemCount = buttons.Count;
        foreach(MenuButton button in buttons)
        {
            button.sceneImage.color = button.normalColor;
        }
        currentMenuItemIndex = 0;
        previousMenuItemIndex = 0;
    }

    void Update()
    {
        GetCurrentMenuItem();
        if (Input.GetMouseButtonDown(0))
            ButtonAction();
    }

    public void GetCurrentMenuItem()
    {
        mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        toVector2M = new Vector2(mousePosition.x / Screen.width, mousePosition.y / Screen.height);

        float angle = (Mathf.Atan2(fromVector2M.y - centerCircle.y, fromVector2M.x - centerCircle.x) 
            - Mathf.Atan2(toVector2M.y - centerCircle.y, toVector2M.x - centerCircle.x)
            * Mathf.Rad2Deg);

        if (angle < 0)
            angle += 360;

        currentMenuItemIndex = (int)(angle / (360 / menuItemCount));

        if(currentMenuItemIndex != previousMenuItemIndex)
        {
            buttons[previousMenuItemIndex].sceneImage.color = buttons[previousMenuItemIndex].normalColor;
            previousMenuItemIndex = currentMenuItemIndex;
            buttons[currentMenuItemIndex].sceneImage.color = buttons[currentMenuItemIndex].highlightColor;
        }
    }

    public void ButtonAction()
    {
        buttons[currentMenuItemIndex].sceneImage.color = buttons[currentMenuItemIndex].pressedColor;
        if (currentMenuItemIndex == 0)
            Debug.Log("You have pressed the first button");
    }

}
