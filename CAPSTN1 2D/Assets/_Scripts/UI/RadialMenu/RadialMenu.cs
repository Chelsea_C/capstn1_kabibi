﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class RadialMenu : MonoBehaviour
    {
        public UnityEngine.UI.Text label;
        public RadialButton buttonPrefab;
        public RadialButton selected;

        public void SpawnButtons(Interactable obj, float delayBetweenButtonSpawn)
        {
            StartCoroutine(AnimateButtons(obj, delayBetweenButtonSpawn));
        }

        public void SpawnButtons(Action[] options, float delayBetweenButtonSpawn)
        {
            StartCoroutine(AnimateButtons(options, delayBetweenButtonSpawn));
        }

        IEnumerator AnimateButtons(Interactable obj, float delayBetweenButtonSpawn)
        {
            for (int i = 0; i < obj.options.Length; i++)
            {
                RadialButton newButton = Instantiate(buttonPrefab) as RadialButton;
                newButton.transform.SetParent(transform, false);
                float theta = (2 * Mathf.PI / obj.options.Length) * i;
                float xPos = Mathf.Sin(theta);
                float yPos = Mathf.Cos(theta);
                newButton.transform.localPosition = new Vector3(xPos, yPos, 0f) * 100f;
                newButton.circle.color = obj.options[i].color;
                newButton.icon.sprite = obj.options[i].sprite;
                newButton.title = obj.options[i].title;
                newButton.myMenu = this;
                newButton.Anim();
                yield return new WaitForSeconds(delayBetweenButtonSpawn);
            }
        }

        IEnumerator AnimateButtons(Action[] options, float delayBetweenButtonSpawn)
        {
            for (int i = 0; i <options.Length; i++)
            {
                RadialButton newButton = Instantiate(buttonPrefab) as RadialButton;
                newButton.transform.SetParent(transform, false);
                float theta = (2 * Mathf.PI / options.Length) * i;
                float xPos = Mathf.Sin(theta);
                float yPos = Mathf.Cos(theta);
                newButton.transform.localPosition = new Vector3(xPos, yPos, 0f) * 100f;
                newButton.circle.color = options[i].color;
                newButton.icon.sprite = options[i].sprite;
                newButton.title = options[i].title;
                newButton.myMenu = this;
                newButton.Anim();
                yield return new WaitForSeconds(delayBetweenButtonSpawn);
            }
        }

        protected void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (selected)
                {
                    Debug.Log(selected.title + " was selected.");
                }
                Destroy(gameObject);
            }
        }
    }
}