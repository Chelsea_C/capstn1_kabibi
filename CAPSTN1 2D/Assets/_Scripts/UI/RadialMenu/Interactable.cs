﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    [System.Serializable]
    public class Action
    {
        public Color color;
        public Sprite sprite;
        public string title;
    }

    public class Interactable : MonoBehaviour
    {
        public string title;
        public float delayBetweenButtonSpawn = 0.06f;
        public Action[] options;

        void Start()
        {
            if (title == "" || title == null)
                title = gameObject.name;
        }

        void OnMouseDown()
        {
            RadialMenuManager.instance.SpawnMenu(this, delayBetweenButtonSpawn);
        }

    }
}