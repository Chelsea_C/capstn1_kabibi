﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class RadialMenuManager : MonoBehaviour
    {
        #region Singleton
        public static RadialMenuManager instance;
        void Awake()
        {
            instance = this;
        }
        #endregion

        public RadialMenu menuPrefab;

        public void SpawnMenu(Interactable obj, float delayBetweenButtonSpawn)
        {
            RadialMenu newMenu = Instantiate(menuPrefab) as RadialMenu;
            newMenu.transform.SetParent(transform, false);
            newMenu.transform.position = Input.mousePosition;
            newMenu.label.text = obj.title.ToUpper();
            newMenu.SpawnButtons(obj, delayBetweenButtonSpawn);
        }

        public void SpawnMenu(Action[] actions, float delayBetweenButtonSpawn)
        {
            RadialMenu newMenu = Instantiate(menuPrefab) as RadialMenu;
            newMenu.transform.SetParent(transform, false);
            newMenu.transform.position = Input.mousePosition;
            newMenu.label.text = "";
            newMenu.SpawnButtons(actions, delayBetweenButtonSpawn);
        }

    }
}
