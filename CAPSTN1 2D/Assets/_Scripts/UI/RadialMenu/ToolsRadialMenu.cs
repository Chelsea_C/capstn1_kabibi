﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class ToolsRadialMenu : RadialMenu
    {
        void OnDestroy()
        {
            Player.instance.IsIssuingTicket = false;
            GameManager.instance.Unpause();
        }

        new void Update()
        {
            if(Input.GetMouseButtonUp(0))
            {
                if (selected && Player.instance.targetVehicle != null)
                {
                    if (selected.title == "Deleter")
                        Player.instance.UseTool(Player.instance.targetVehicle, selected.title);
                    else
                        Player.instance.IssueTicket(Player.instance.targetVehicle, selected.title);
                }

                Destroy(gameObject);
            }

        }

    }
}
