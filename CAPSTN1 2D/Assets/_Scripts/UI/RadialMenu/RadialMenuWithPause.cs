﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class RadialMenuWithPause : RadialMenu
    {
        void OnDestroy()
        {
            GameManager.instance.Unpause();
        }

    }
}
