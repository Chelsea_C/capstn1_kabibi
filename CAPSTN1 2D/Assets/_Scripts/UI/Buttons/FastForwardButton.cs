﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Button))]
    public class FastForwardButton : MonoBehaviour
    {
        public Color activeColor;
        Color original;

        void Start()
        {
            GetComponent<Button>().onClick.AddListener(GameManager.instance.SpeedUp);
            original = GetComponent<Image>().color;
        }

        void Update()
        {
            if (GameManager.instance.mSpeed == GAME_SPEED.FAST)
                GetComponent<Image>().color = activeColor;
            else
                GetComponent<Image>().color = original;
        }
    }
}
