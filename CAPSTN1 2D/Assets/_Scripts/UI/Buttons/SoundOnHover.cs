﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SoundOnHover : MonoBehaviour, IPointerEnterHandler
{
    public AudioSource sound;

    void Start()
    {
        if (sound == null)
            sound = this.gameObject.AddComponent<AudioSource>();

        if (Urbana.AudioManager.instance != null)
            sound.clip = Urbana.AudioManager.instance.data.buttonHover;
        if (Urbana.GameManager.instance != null)
            sound.clip = Urbana.GameManager.instance.mData.buttonHover;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (sound != null && sound.clip != null)
            sound.Play();
    }
}
