﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Button))]
    public class PauseButton : MonoBehaviour
    {
        void Start()
        {
            GetComponent<Button>().onClick.AddListener(LevelManager.instance.PauseLevel);
        }
    }
}
