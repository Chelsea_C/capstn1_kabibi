﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Button))]
    public class GoToLevelSelect : MonoBehaviour
    {
        void Start()
        {
            GetComponent<Button>().onClick.AddListener(Activate);
        }

        public void Activate()
        {
            Initiate.Fade("LevelSelect", Color.black, 1f);
        }
    }
}