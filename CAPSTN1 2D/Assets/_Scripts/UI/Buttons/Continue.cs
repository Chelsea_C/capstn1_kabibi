﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace Urbana
{
    [RequireComponent(typeof(Button))]
    public class Continue : MonoBehaviour
    {
        void Start()
        {
            GetComponent<Button>().onClick.AddListener(Activate);
        }

        public void Activate()
        {
            LevelManager.instance.UnpauseLevel();
        }
    }
}