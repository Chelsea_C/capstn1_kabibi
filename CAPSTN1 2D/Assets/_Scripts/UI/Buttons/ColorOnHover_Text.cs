﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class ColorOnHover_Text : MonoBehaviour
{
    public Color normalColor;
    public Color hoverColor;

    TMP_Text text;

    void Start()
    {
        text = GetComponent<TMP_Text>();
        normalColor = text.color;
    }

    public void ActivateHoverColor()
    {
        text.color = hoverColor;
    }

    public void DeactivateHoverColor()
    {
        text.color = normalColor;
    }
}
