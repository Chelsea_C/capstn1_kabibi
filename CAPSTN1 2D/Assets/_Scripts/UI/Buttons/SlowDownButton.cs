﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace Urbana
{
    [RequireComponent(typeof(Button))]
    public class SlowDownButton : MonoBehaviour
    {
        public Color activeColor;
        Color original;

        void Start()
        {
            GetComponent<Button>().onClick.AddListener(GameManager.instance.SlowDown);
            original = GetComponent<Image>().color;
        }

        void Update()
        {
            if (GameManager.instance.mSpeed == GAME_SPEED.SLOW)
                GetComponent<Image>().color = activeColor;
            else
                GetComponent<Image>().color = original;
        }

    }
}