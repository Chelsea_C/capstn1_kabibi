﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Button))]
    public class GoToMainMenu : MonoBehaviour
    {
        void Start()
        {
            GetComponent<Button>().onClick.AddListener(Activate);
        }

        public void Activate()
        {
            Initiate.Fade("MainMenu", Color.black, 1f);
        }
    }
}
