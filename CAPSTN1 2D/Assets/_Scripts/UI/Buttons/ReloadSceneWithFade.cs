﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Button))]
    public class ReloadSceneWithFade : MonoBehaviour
    {
        void Start()
        {
            GetComponent<Button>().onClick.AddListener(Activate);
        }

        public void Activate()
        {
            Initiate.Fade(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name, Color.black, 1f);
        }
    }
}