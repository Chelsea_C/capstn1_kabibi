﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.SceneManagement;

namespace Urbana
{
    [RequireComponent(typeof(Button))]
    public class LevelSelectButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public LevelData levelData;
        public Image sprite_lock;
        public Image sprite_star;
        public Image fill_outer;
        public Image fill_middle;
        public Image fill_inner;
        public TMP_Text number;
        public TMP_Text highScore;

        bool ringsCompleted;

        Button button;

        void Start()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(LoadLevel);
            number.text = levelData.LEVEL_NUMBER.ToString();
            ringsCompleted = false;
        }

        void Update()
        {
            if (levelData.IsUnlocked)
                sprite_lock.gameObject.SetActive(false);

            if (levelData.HIGH_SCORE > 0)
                highScore.text = "Best: " + levelData.HIGH_SCORE.ToString();

            sprite_star.gameObject.SetActive(ringsCompleted);
            UpdateRings();
        }

        void UpdateRings()
        {
            switch (levelData.GetStarsUnlocked)
            {
                case 0:
                    fill_outer.fillAmount = 0;
                    fill_middle.fillAmount = 0;
                    fill_inner.fillAmount = 1 - (float)(levelData.oneStarMinimum - levelData.HIGH_SCORE) / (float)levelData.oneStarMinimum;
                    break;
                case 1:
                    fill_outer.fillAmount = 0;
                    fill_middle.fillAmount = 1 - (float)(levelData.twoStarMinimum - levelData.HIGH_SCORE) / (float)(levelData.twoStarMinimum - levelData.oneStarMinimum);
                    fill_inner.fillAmount = 1;
                    break;
                case 2:
                    fill_outer.fillAmount = 1 - (float)(levelData.threeStarMinimum - levelData.HIGH_SCORE) / (float)(levelData.threeStarMinimum - levelData.twoStarMinimum);
                    fill_middle.fillAmount = 1;
                    fill_inner.fillAmount = 1;
                    break;
                case 3:
                    fill_outer.fillAmount = 1;
                    fill_middle.fillAmount = 1;
                    fill_inner.fillAmount = 1;
                    ringsCompleted = true;
                    break;
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            //selection.GetComponent<LevelSelect>().MoveSelectionTo(this);
            //selection.Unhide();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            //selection.Hide();
        }

        void LoadLevel()
        {
            if (!levelData.IsUnlocked)
                return;

            if (levelData.INSTRUCTIONS_SCENE_NAME != "")// && !levelData.InstructionsCompleted)
            {
                LoadInstructions();
                return;
            }

            SceneHandler.instance.GoToSceneWithFade(levelData.SCENE_NAME);
        }

        void LoadInstructions()
        {
            SceneHandler.instance.GoToSceneWithFade(levelData.INSTRUCTIONS_SCENE_NAME);
        }
    }
}
