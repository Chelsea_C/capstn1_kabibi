﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Text))]
    public class DayDisplay : MonoBehaviour
    {
        public Image color1;
        public Image color2;
        Text display;

        void Start()
        {
            display = GetComponent<Text>();
        }

        void Update()
        {
            display.text = LevelManager.instance.currentDay.ToString();
            DisplayColors();
        }

        void DisplayColors()
        {
            List<int> banned = LevelManager.instance.GetBannedNumbers(LevelManager.instance.currentDay);
            color1.color = LevelManager.instance.GetColor(banned[0]);
            color2.color = LevelManager.instance.GetColor(banned[1]);
        }
    }
}