﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Text))]
    public class LevelDisplay : MonoBehaviour
    {
        public bool DisplayWithLabel = false;
        Text display;

        void Start()
        {
            display = GetComponent<Text>();
        }

        void Update()
        {
            if(!DisplayWithLabel)
                display.text = LevelManager.instance.currentLevelNumber.ToString();
            else
                display.text = "Level " + LevelManager.instance.currentLevelNumber.ToString();
        }
    }
}
