﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    public class TimerDisplay : MonoBehaviour
    {
        Image display;

        void Start()
        {
            display = GetComponent<Image>();
            LevelManager.instance.sprite_Timer = this.display;
        }

        void Update()
        {
            if (LevelManager.instance.sprite_Timer == null)
                LevelManager.instance.sprite_Timer = this.display;
        }
    }
}