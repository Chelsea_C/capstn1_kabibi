﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Urbana
{
    [RequireComponent(typeof(Text))]
    public class ScoreDisplay : MonoBehaviour
    {
        Text display;
        void Start()
        {
            display = GetComponent<Text>();
        }

        void Update()
        {
            display.text = LevelManager.instance.score.ToString();
        }
    }
}
