﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Urbana
{
    public class MainMenu : MonoBehaviour
    {
        public AudioSource click;
        public AudioSource bgm;
        public AudioSource ambience;

        void Start()
        {
            ambience.loop = true;
            ambience.Play();
            
            bgm.loop = true;
            bgm.Play();
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0) && click.clip != null)
                click.Play();
        }
    }
}
