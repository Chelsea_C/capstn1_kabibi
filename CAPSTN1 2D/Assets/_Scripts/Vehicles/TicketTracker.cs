﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class TicketTracker : MonoBehaviour
    {
        public List<SpriteRenderer> marks;
        public int markCount = 0;

        void Start()
        {
            foreach (SpriteRenderer mark in marks)
            {
                mark.sortingOrder = GetComponentInParent<Vehicle>().licensePlate.sortingOrder;
                Disable(mark);
            }

            markCount = 0;
        }
        
        void Update()
        {

        }

        public void Disable(SpriteRenderer sr)
        {
            sr.enabled = false;
        }

        public void Enable (SpriteRenderer sr)
        {
            sr.enabled = true;
        }

        public void ActivateMark(Color color)
        {
            Debug.Log("Activating " + (markCount+1));
            marks[markCount].color = color;
            Enable(marks[markCount]);
            markCount++;
        }

    }
}