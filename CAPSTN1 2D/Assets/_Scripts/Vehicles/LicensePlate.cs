﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class LicensePlate : MonoBehaviour
    {
        [SerializeField][ReadOnly]
        SpriteRenderer sprite;
        [SerializeField][ReadOnly]
        SpriteRenderer vehicle;

        void Start()
        {
            sprite = GetComponent<SpriteRenderer>();
            vehicle = GetComponentInParent<Vehicle>().GetComponent<SpriteRenderer>();
        }
        
        void Update()
        {
            if(sprite.sortingOrder != vehicle.sortingOrder + 1)
                sprite.sortingOrder = vehicle.sortingOrder + 1;
        }
    }
}