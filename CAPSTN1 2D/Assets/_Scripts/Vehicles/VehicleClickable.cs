﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Urbana
{
    public class VehicleClickable : MonoBehaviour
    {
        void OnMouseEnter()
        {
            if (LevelManager.instance.HasEnded)
                return;

            CursorManager.instance.SetTicketCursor();
        }

        void OnMouseDown()
        {
            if (LevelManager.instance.HasEnded)
                return;

            Player.instance.SetTargetVehicle(this.transform.GetComponent<Vehicle>());
            Player.instance.IsIssuingTicket = true;
            LevelManager.instance.SpawnToolMenu();
        }

        void OnMouseExit()
        {
            if (LevelManager.instance.HasEnded)
                return;

            if (!Player.instance.IsIssuingTicket)
            {
                Player.instance.ClearTargetVehicle();
                CursorManager.instance.SetDefaultCursor();
            }
        }

    }
}
