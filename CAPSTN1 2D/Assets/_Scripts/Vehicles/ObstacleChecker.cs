﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    [RequireComponent(typeof(Vehicle))]
    public class ObstacleChecker : MonoBehaviour
    {
        public Obstacle currentObstacle;
        public List<Obstacle> ignoredObstacles;

        Vehicle vehicle;

        void Start()
        {
            vehicle = GetComponent<Vehicle>(); 
        }

        public bool IsPassible()
        {
            if (currentObstacle == null)
                return true;

            if (currentObstacle is NoStopZone)
            {
                return currentObstacle.GetComponent<NoStopZone>().hasForwardRoom;
            }

            if (currentObstacle is Stoplight)
            {
                if (GetComponent<Vehicle>().IsInIntersection)
                    return true;

                WayPoint exit = GetExitNode(); // Add a null check for this

                Stoplight stoplight = currentObstacle.GetComponent<Stoplight>();
                if (stoplight.state == LIGHT_STATE.GO && !exit.GetComponentInChildren<SpaceChecker>().isOccupied)
                    return true;
            }

            return false;
        }

        bool IsIgnored(Obstacle _obstacle)
        {
            for (int i = 0; i < ignoredObstacles.Count; i++)
            {
                if(ignoredObstacles[i].name == _obstacle.name)
                    return true;
            }
            return false;
        }

        public void Clear()
        {
            currentObstacle = null;
        }

        // Vehicle will either STATE.CHECK_OBSTACLE or ignore it and keep STATE.MOVING
        public VEHICLE_STATE MakeDecision(Obstacle _obstacle)
        {

            if (IsIgnored(_obstacle))
                return VEHICLE_STATE.MOVING;

            if (vehicle.AlwaysFollowLights && _obstacle is Stoplight)
                return VEHICLE_STATE.CHECKING_OBSTACLE;

            if (vehicle.AlwaysFollowNoStopZone && _obstacle is NoStopZone)
                return VEHICLE_STATE.CHECKING_OBSTACLE;


            /* If randomized number is higher than or equal to the 
             minimumToBreakRules, the vehicle will break rules. */
            float rolledNumber = Random.Range(0, 100);
            float minToBreakRules = GameManager.instance.GetMinimumToBreakRules(GetComponent<Vehicle>().style);
            if (rolledNumber < minToBreakRules)
            {
                return VEHICLE_STATE.CHECKING_OBSTACLE;
            }
            else
            {
                return VEHICLE_STATE.MOVING;
            }
        }

        WayPoint GetExitNode()
        {
            var path = GetComponent<FindPath>();
            for (int i = 0; i < path.route.Count; i++)
            {
                if (path.route[i].isInIntersectionExit)
                    if (!path.route[i + 1].isInIntersectionExit)
                        return path.route[i];
            }
            return null;
        }

        public void AddIgnoredObstacle(Obstacle _obstacle)
        {
            ignoredObstacles.Add(_obstacle);
        }

    }
}
