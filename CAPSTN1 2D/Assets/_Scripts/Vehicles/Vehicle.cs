﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace Urbana
{ 
    public enum VEHICLE_STATE
    {
        MOVING,
        IDLE,
        CHECKING_OBSTACLE,
        WAIT
    }

    public enum VEHICLE_STYLE
    {
        EVIL, // for debugging, always breaks rules
        SAINT, // for debugging, never breaks rules
        AGGRESSIVE,
        NORMAL,
        PASSIVE        
    }

    public class Vehicle : MonoBehaviour
    {
        //public Transform sprite;

        public float maxSpeed;
        public Color sensorColor = Color.magenta;
        public float sensorLength = 3f;

        public bool DestroyAtDestination = true;
        //public float frontSensorStartPoint = 5;
        //public float frontSensorSideDistance = 2;
        //public float frontSensorsAngle = 30;
        //public float sidewaySensorLength = 5;


        [Header("Vehicle Info")]
        public SpriteRenderer licensePlate;
        public int licenseEnding;
        public VEHICLE_STATE state = VEHICLE_STATE.MOVING;
        public VEHICLE_STYLE style = VEHICLE_STYLE.NORMAL;
        public int cluster = 0;


        [Header("Pathing and Obstacles")]
        public List<Obstacle> checkedObstacles;
        public LayerMask obstaclesLayer;
        public LayerMask vehiclesLayer;
        public float timeUntilDestroyed = 3f;
        public Intersection currentIntersection = null;
        public bool IsInIntersection = false;
        public Vehicle carInFront;
        public VehicleChecker vehicleChecker;

        [Header("Force Obstacle Decisions")]
        public bool AlwaysFollowLights = false;
        public bool AlwaysFollowNoStopZone = false;


        [Header("Violations")]
        public List<Violation> violations;
        public TicketTracker tickets;

        [Header("Sensors")]
        public Transform startPos;
        public Transform rightPos;
        public Transform leftPos;
        public float xOffset;
        public float fovAngle = 30;

        [SerializeField]
        public List<RaycastHit> obstacleHits;

        RaycastHit hitMid;
        RaycastHit hitRight;
        RaycastHit hitLeft;
        RaycastHit hitSideRight;
        RaycastHit hitSideLeft;

        Ray rayCenter;
        Ray rayLeft;
        Ray rayRight;

        SpriteRenderer spriteRenderer;

        //public float frontSensorPosition = 0.09f;
        //public float sensorLength = .01f;

        protected FindPath path;
        protected ObstacleChecker obstacle;

        protected void Start()
        {
            gameObject.tag = "Vehicle";

            state = VEHICLE_STATE.MOVING;

            spriteRenderer = this.GetComponent<SpriteRenderer>();

            IsInIntersection = false;

            path = GetComponent<FindPath>();
            obstacle = GetComponent<ObstacleChecker>();
            tickets = GetComponentInChildren<TicketTracker>();
        }

        protected void Update()
        {
            if (GameManager.instance.mState == GAME_STATE.PAUSE)
                return;

            //Sensors();
            ObstacleSensors();
            VehicleSensors();

            State();
            TrackViolations();



            //DrawRays();
            Debug.DrawRay(leftPos.position, rayLeft.direction * sensorLength, Color.yellow);
            Debug.DrawRay(rightPos.position, rayRight.direction * sensorLength, Color.green);
        }

        protected void FixedUpdate()
        {
            if (GameManager.instance.mState == GAME_STATE.PAUSE)
                return;

            MoveTowardDestination();
        }

        protected void DrawRays()
        {
            Vector3 leftRayRotation = Quaternion.AngleAxis(fovAngle, transform.forward) * transform.up;
            Vector3 rightRayRotation = Quaternion.AngleAxis(-fovAngle, transform.forward) * transform.up;


            rayCenter = new Ray(startPos.position, transform.up);
            rayLeft = new Ray(leftPos.position, leftRayRotation);
            rayRight = new Ray(rightPos.position, rightRayRotation);

        }

        protected void State()
        {
            switch (state)
            {
                case VEHICLE_STATE.IDLE:
                    path.isPaused = true;
                    break;

                case VEHICLE_STATE.MOVING:
                    path.isPaused = false;
                    break;

                case VEHICLE_STATE.CHECKING_OBSTACLE:
                    path.isPaused = true;
                    if (obstacle.IsPassible())
                    {
                        state = VEHICLE_STATE.MOVING;
                        obstacle.Clear();
                    }
                    break;

                case VEHICLE_STATE.WAIT:
                    path.isPaused = true;
                    if (NoVehicleAhead())
                        state = VEHICLE_STATE.MOVING;
                    break;
            }
        }



        #region Sensing obstacles and cars

        protected void Sensors()
        {

            Debug.DrawRay(startPos.position, transform.up * sensorLength, Color.cyan);

            if (path.hasPath && !path.isCompleted)
            {
                if (Physics.Raycast(startPos.position, transform.up, out hitMid, (sensorLength + 0.5f)))
                {
                    if (state != VEHICLE_STATE.CHECKING_OBSTACLE && hitMid.collider.tag == "Vehicle")
                    {
                        carInFront = hitMid.collider.GetComponent<Vehicle>();
                        //Debug.Log(name + ": Car detected!");
                        //state = VEHICLE_STATE.WAIT;
                        state = VEHICLE_STATE.IDLE;
                        if (!hitMid.collider.GetComponent<Vehicle>().IsInIntersection && IsInIntersection)
                            state = VEHICLE_STATE.MOVING;
                        else
                            if (!IsInIntersection)
                            StartCoroutine(WaitForCarToPass());
                        else
                            StartCoroutine(WaitForTurnToMove());
                    }

                    if (hitMid.collider.tag == "Obstacle")
                    {
                        var obs = hitMid.collider.GetComponent<Obstacle>();
                        if (HasBeenChecked(obs))
                            return;

                        //Debug.Log(name + ": Obstacle " + obs.name + " detected!");
                        checkedObstacles.Add(obs);
                        obstacle.currentObstacle = obs;
                        state = obstacle.MakeDecision(obs);
                    }
                }
                else
                    state = VEHICLE_STATE.MOVING;


                //if (Physics.Raycast(leftPos.position, rayLeft.direction, out hitLeft, sensorLength))
                //{
                //    if (state != VEHICLE_STATE.CHECKING_OBSTACLE && hitLeft.collider.tag == "Vehicle")
                //    {
                //        state = VEHICLE_STATE.IDLE;
                //        StartCoroutine(WaitForCarToPass(hitLeft.collider.GetComponent<Vehicle>()));
                //    }
                //}

                //if (Physics.Raycast(rightPos.position, rayRight.direction, out hitRight, sensorLength))
                //{
                //    if (state != VEHICLE_STATE.CHECKING_OBSTACLE && hitRight.collider.tag == "Vehicle")
                //    {
                //        //Debug.Log(name + ": Car detected!");
                //        //state = VEHICLE_STATE.WAIT;
                //        state = VEHICLE_STATE.IDLE;
                //        StartCoroutine(WaitForCarToPass(hitRight.collider.GetComponent<Vehicle>()));
                //    }
                //}

            }
        }

        protected void VehicleSensors()
        {
            cluster = GetComponent<SpriteRenderer>().sortingOrder;

            if (!vehicleChecker.isOccupied)
                return;

            if (IsInIntersection && !vehicleChecker.occupant.IsInIntersection)
                return;

            if (vehicleChecker.occupant.transform.GetComponent<Vehicle>().cluster == this.transform.GetComponent<Vehicle>().cluster)
            {
                //Debug.Log(vehicleChecker.occupant);
                state = VEHICLE_STATE.WAIT;
                carInFront = vehicleChecker.occupant;
            }


            //Debug.DrawRay(startPos.position, transform.up * sensorLength, Color.cyan);
            //if (path.hasPath && !path.isCompleted && state == VEHICLE_STATE.MOVING)
            //{
            //    if (Physics.Raycast(startPos.position, transform.up, out hitMid, (sensorLength + 0.5f), vehiclesLayer))
            //    {
            //        if (hitMid.collider.tag == "Vehicle" && IsInIntersection && !hitMid.transform.GetComponent<Vehicle>().IsInIntersection)
            //            return;

            //        if (hitMid.collider.tag == "Vehicle" && hitMid.transform.GetComponent<SpriteRenderer>().sortingOrder == GetComponent<SpriteRenderer>().sortingOrder)
            //        {
            //            state = VEHICLE_STATE.WAIT;
            //            carInFront = hitMid.collider.GetComponent<Vehicle>();
            //        }
            //    }
            //}
        }

        protected void ObstacleSensors()
        {
            Debug.DrawRay(startPos.position, transform.up * (sensorLength + 0.1f), Color.red);
            if (path.hasPath && !path.isCompleted && state == VEHICLE_STATE.MOVING)
            {
                //Debug.Log("Checking for Obstacles...");
                if (Physics.Raycast(startPos.position, transform.up, out hitMid, (sensorLength + 0.1f), obstaclesLayer))
                {

                    if (hitMid.collider.tag == "Obstacle")
                    {
                        var obs = hitMid.collider.GetComponent<Obstacle>();
                        if (HasBeenChecked(obs))
                            return;

                        //Debug.Log(name + ": Obstacle " + obs.name + " detected!");
                        checkedObstacles.Add(obs);
                        obstacle.currentObstacle = obs;
                        state = obstacle.MakeDecision(obs);
                    }
                }
            }
        }

        protected IEnumerator WaitForCarToPass()
        {
            if (state == VEHICLE_STATE.WAIT)
                yield return null;

            while (true)
            {
                //if (IsInIntersection && currentIntersection != null)
                //     //lower index means higher priority because it was added/got to intersection first
                //    if (currentIntersection.GetPriorityInQueue(this) < currentIntersection.GetPriorityInQueue(other))
                //    {
                //        state = VEHICLE_STATE.MOVING;
                //        yield return null;
                //        StopCoroutine(WaitForCarToPass());
                //    }
                state = VEHICLE_STATE.WAIT;
                yield return new WaitForSeconds(5f);
                state = VEHICLE_STATE.MOVING;
                StopCoroutine(WaitForCarToPass());
            }
        }

        protected IEnumerator WaitForTurnToMove()
        {
            while (true)
            {
                state = VEHICLE_STATE.WAIT;
                yield return new WaitForSeconds(1f);
                if (currentIntersection.vehicleQueue[0] == this)
                {
                    state = VEHICLE_STATE.MOVING;
                    StopCoroutine(WaitForTurnToMove());
                }
            }
        }

        protected bool NoVehicleAhead()
        {
            if (vehicleChecker.isOccupied)
                return true;
            else
                return false;

            //if (Physics.Raycast(startPos.position, transform.up, out hitMid, sensorLength + 0.5f, 1 << vehiclesLayer))
            //{
            //    if (hitMid.collider.tag == "Vehicle")
            //        return false;
            //}
            //return true;
        }

        protected bool HasBeenChecked(Obstacle obstacle)
        {
            foreach (Obstacle o in checkedObstacles)
                if (obstacle == o)
                    return true;
            return false;
        }
        #endregion

        #region Moving and Reaching Destination
        protected void MoveTowardDestination()
        {
            path.MoveAlongPath();
            if (DestroyAtDestination)
                OnReachDestination();
        }

        public void SetSpeed(float _speed)
        {
            path.SetSpeed(_speed);
        }

        protected void OnReachDestination()
        {
            if (path.isCompleted)
                StartCoroutine(Despawn());
        }

        protected IEnumerator Despawn()
        {
            yield return new WaitForSeconds(timeUntilDestroyed);
            if (LevelManager.instance != null)
            {
                LevelManager.instance.vehicles.Remove(this);
            }
            Destroy(gameObject);
        }

        public void FadeAndDespawn(float duration)
        {
            StartCoroutine(FadingDespawn(duration));
        }

        protected IEnumerator FadingDespawn(float duration)
        {
            licensePlate.DOFade(0, duration);
            spriteRenderer.DOFade(0, duration);
            foreach (SpriteRenderer sr in tickets.marks)
                sr.DOFade(0, duration);

            yield return new WaitForSeconds(duration);
            if (LevelManager.instance != null)
            {
                LevelManager.instance.vehicles.Remove(this);
            }
            Destroy(gameObject);
        }

        #endregion

        #region Activate Checkers

        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Checker")
            {
                other.gameObject.GetComponent<SpaceChecker>().isOccupied = true;
                other.gameObject.GetComponent<SpaceChecker>().currentOccupant = this;
            }

            if (other.tag == "Intersection")
                IsInIntersection = true;

            if (other.tag == "Vehicle")
            {
               // if in another vehicle...
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.tag == "Checker")
            {
                other.gameObject.GetComponent<SpaceChecker>().ClearVehicle();
                //other.gameObject.GetComponent<SpaceChecker>().isOccupied = false;
            }

            if (other.tag == "Intersection")
                IsInIntersection = false;
        }

        #endregion

        #region Violations

        void CheckCoding()
        {
            var day = LevelManager.instance.currentDayIndex;
        }

        void TrackViolations()
        {
            var vios = GetComponentsInChildren<Violation>();
            if(vios.Length != violations.Count)
            {
                violations.Clear();
                foreach (Violation v in vios)
                    violations.Add(v);
            }
        }

        public void AddViolation(Violation violation)
        {
            violations.Add(violation);
        }
        #endregion
    }
}
