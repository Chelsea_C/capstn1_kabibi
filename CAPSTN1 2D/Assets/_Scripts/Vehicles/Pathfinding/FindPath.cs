﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FindPath : MonoBehaviour {

    public List<WayPoint> route = new List<WayPoint>();

    public WayPoint start;
    public WayPoint end;

    public bool hasPath = false;
    public bool isPaused = false;
    public bool isCompleted = false;

    public int nextNodeIndex;
    private Transform t;

    [SerializeField]
    float speed = 1f;

    /* Initialization */
    void Start()
    {
        //if (start != null && end != null && !hasPath)
        //{
        //    route = FindRouteTo(start, end);
        //    hasPath = true;
        //}

        //if (start == null && end != null && !hasPath)
        //{
        //    //SetPositionAsStartPoint(end);
        //    MoveTowardEndPoint();
        //    hasPath = true;
        //}
                       
        t = this.gameObject.transform;
    }

    /* Move the object */
    int i = 0;
	void Update () {

        /*
        //leave in case we reached our destination
        if (i >= route.Count) return;
        //Distance between the square and the current target waypoint
        float distance = Vector3.Distance(t.position, route[i].transform.position);
        //Move the square towards the current target
        t.position = Vector3.Lerp(t.position, route[i].transform.position, speed * Time.deltaTime / distance);
        //In case the square arrived to the target waypoint (very small distance)
        if (distance < 0.1)
            //Change the current target to the next defined waypoint
            i++;*/
    }

    public void SetSpeed(float _speed)
    {
        speed = _speed;
    }

    public void MoveAlongPath()
    {
        speed = GetComponent<Urbana.Vehicle>().maxSpeed;
        if (!hasPath)
            return;

        //leave in case we reached our destination
        if (i >= route.Count)
        {
            isCompleted = true;
            return;
        }
        
        //Distance between the square and the current target waypoint
        float distance = Vector3.Distance(t.position, route[i].transform.position);
        //Move the square towards the current target
        if (!isPaused)
        {
            RotateTowardsTarget(route[i].transform.position);
            nextNodeIndex = i;
            t.position = Vector3.Lerp(t.position, route[i].transform.position, speed * Time.deltaTime / distance);
        }
        //In case the square arrived to the target waypoint (very small distance)
        if (distance < 0.05)
            //Change the current target to the next defined waypoint
            i++;
    }

    public void MoveTowardEndPoint()
    {
        speed = GetComponent<Urbana.Vehicle>().maxSpeed;
        float distance = Vector3.Distance(transform.position, end.transform.position);
        
        if (!isPaused)
        {
            RotateTowardsTarget(end.transform.position);
            transform.position = Vector3.Lerp(transform.position, end.transform.position, speed * Time.deltaTime / distance);
        }
        //In case the square arrived to the target waypoint (very small distance)
        if (distance < 0.05)
        {
            isCompleted = true;
            return;
        }
    }

    public List<WayPoint> FindRouteTo(WayPoint Start, WayPoint End) {
        //We will store the path through a dictionary to keep track of where we came from to that point
        //and to keep track of the visited waypoints
        Dictionary<WayPoint, WayPoint> d= new Dictionary<WayPoint, WayPoint>();
        //first we save the root as visited in our dictionary
        d.Add(Start, null);

        //BFS to find the path with least nodes in between to our target
        Queue<WayPoint> q = new Queue<WayPoint>();
        q.Enqueue(Start);
        while (q.Count > 0) {
            WayPoint current = q.Dequeue();
            if (current == null)
                continue;
            foreach(WayPointPercent w in current.outs) {
                if (!d.ContainsKey(w.waypoint)) {
                    q.Enqueue(w.waypoint);
                    d.Add(w.waypoint, current);
                    //Debug.Log(w.waypoint + ", " + current);
                }
            }
        }



        //Now we have to translate from dictionary to list
        List<WayPoint> result = new List<WayPoint>();
        //start retrieving the path from the end
        WayPoint i = End;
        //until we find a node without origin (the starting node)
        while (i != null) {
            //insert at the beggining of the list
            result.Insert(0, i);

            //go to the next node
            i = d[i];

            //WayPoint wp = new WayPoint();
            //if (d.TryGetValue(i, out wp))
            //{
            //    Debug.Log("Key found");
            //    i = wp;
            //}
            //else
            //{
            //    Debug.Log("Key not found");
            //}
        }

        return result;
    }

    void RotateTowardsTarget(Vector3 target)
    {
        float rotationSpeed = 10f;
        float offset = -90f;
        Vector3 direction = target - transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle + offset, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
    }

    public void SetStartAndEndPoints(WayPoint _start, WayPoint _end)
    {
        start = _start;
        end = _end;
        route = FindRouteTo(start, end);
        hasPath = true;
    }

    public void SetPositionAsStartPoint(WayPoint _end)
    {
        WaypointCluster parent = _end.getParent();
        WayPoint start = parent.CreateWaypoint(transform.position);
        start.addOutWayPoint(end);
        end = _end;
        route = FindRouteTo(start, end);
        hasPath = true;
    }
}
