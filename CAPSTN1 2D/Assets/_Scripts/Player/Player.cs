﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public class Player : MonoBehaviour
    {
        #region Singleton
        public static Player instance;
        void Awake()
        {
            instance = this;
        }
        #endregion

        public PlayerData data;
        public int _points;

        [Header("Ticketing")]
        public bool IsMouseOverVehicle = false;
        public bool ticketTrigger = false;
        public Vehicle targetVehicle;

        public bool IsIssuingTicket = false;

        void Start()
        {
            _points = 0;
        }

        void Update()
        {
            if (LevelManager.instance.levelEnd.gameObject.activeSelf)
                return;

            #region Pausing
            if (!LevelManager.instance.HasStarted)
                return;

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Escape))
            {
                switch (GameManager.instance.mState)
                {
                    case GAME_STATE.PLAY:
                        LevelManager.instance.PauseLevel();
                        break;
                    case GAME_STATE.PAUSE:
                        LevelManager.instance.UnpauseLevel();
                        break;
                }
            }
            #endregion

            #region Check Mouse Hold Over Vehicle

            //if (Input.GetMouseButton(0) && IsMouseOverVehicle)
            //{
            //    //ticketTrigger = true;
            //    state = GAME_STATE.PAUSED;
            //    Debug.Log("Clicked on vehicle: " + targetVehicle.name);
            //}

            //if (ticketTrigger)
            //    state = GAME_STATE.PAUSED;
            //else
            //    state = GAME_STATE.PLAY;

            //if(ticketTrigger)
            //{
            //    Debug.Log("Ticketing!");
            //    ticketTrigger = false;
            //}
            #endregion

        }

        public void Pause()
        {

        }

        public void Unpause()
        {

        }

        public void RemovePoints(int points)
        {
            _points -= points;
        }

        public void AddPoints(int points)
        {
            if(targetVehicle != null)
                FloatingTextManager.instance.CreateFloatingText("+" + points.ToString(), targetVehicle.transform, FLOATING_TYPE.POINTS);
            _points += points;
        }

        public void SetTargetVehicle(Vehicle vehicle)
        {
            targetVehicle = vehicle;
        }

        public void ClearTargetVehicle()
        {
            targetVehicle = null;
        }

        #region Ticketing

        public void IssueTicket(Vehicle _vehicle, string _violation)
        {
            if(_vehicle.violations.Count <= 0)
            {
                Debug.Log(_vehicle.name + " has no violations.");
                AudioManager.instance.PlayIncorrectTicket();
                //if (targetVehicle != null)
                //    FloatingTextManager.instance.CreateFloatingText("*@#!", targetVehicle.transform, FLOATING_TYPE.CURSE);
                return;
            }

            var violation = LevelManager.instance.GetViolation(_violation);
            foreach (Violation v in _vehicle.violations)
            {
                if (violation.GetComponent<Violation>().name == v.data.name && !v.HasBeenTicketed)
                {
                    AddPoints(v.data.maxReward);
                    AudioManager.instance.PlayCorrectTicket();
                    v.HasBeenTicketed = true;

                    if(_vehicle.tickets.markCount < 3)
                        _vehicle.tickets.ActivateMark(v.data.color);

                }
            }
        }

        #endregion

        public void UseTool(Vehicle _vehicle, string _name)
        {
            if (_name == "Deleter")
            {
                Debug.Log("DELETING");
                //_vehicle.StartDespawn(1.5f);
                LevelManager.instance.vehicles.Remove(_vehicle);
                _vehicle.FadeAndDespawn(2.0f);
                return;
            }
        }
    }
}
