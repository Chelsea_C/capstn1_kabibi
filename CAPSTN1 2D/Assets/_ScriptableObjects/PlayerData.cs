﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    [CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObject/PlayerData")]
    public class PlayerData : ScriptableObject
    {
        public List<LevelStats> levelStats;
        public List<Level> levels;

    }
}
