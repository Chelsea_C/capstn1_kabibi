﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    [CreateAssetMenu(fileName = "New ToolData", menuName = "ScriptableObject/ToolData")]
    public class ToolData : ScriptableObject
    {
        public new string name;
        public Sprite icon;
        public Color color;
    }
}