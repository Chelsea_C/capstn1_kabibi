﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    [CreateAssetMenu(fileName = "New ViolationData", menuName = "ScriptableObject/ViolationData")]
    public class ViolationData : ScriptableObject
    {
        public new string name;
        public Sprite icon;
        public Sprite markIcon;
        public Color color;
        public float violationDuration;
        public float apprehensionDuration;
        public int maxReward;
        public int minReward;

    }
}
