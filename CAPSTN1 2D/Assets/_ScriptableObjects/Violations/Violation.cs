﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    public enum VIOLATION_STATE
    {
        INACTIVE,
        ACTIVE,
        COUNTDOWN,
        TIMED_OUT,
        TICKETED
    }

    public class Violation : MonoBehaviour
    {
        public VIOLATION_STATE state;
        public ViolationData data;
        public bool DestroyOnTimeOut = false;
        public bool HasBeenTicketed = false;
        
        void Update()
        {
            switch (state)
            {
                case VIOLATION_STATE.INACTIVE:
                    break;
                case VIOLATION_STATE.ACTIVE:
                    state = VIOLATION_STATE.COUNTDOWN;
                    break;
                case VIOLATION_STATE.COUNTDOWN:
                    if (data.apprehensionDuration <= 0)
                        break;

                    CountdownManager.instance.QueueCountdown(this);
                    break;

                case VIOLATION_STATE.TIMED_OUT:
                    if(DestroyOnTimeOut)
                        Destroy(this.gameObject);
                    break;
                case VIOLATION_STATE.TICKETED:
                    HasBeenTicketed = true;
                    break;
            }
        }
    }
}
