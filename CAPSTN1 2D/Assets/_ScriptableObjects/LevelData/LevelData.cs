﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Urbana
{
    [CreateAssetMenu(fileName = "New LevelData", menuName = "ScriptableObject/LevelData")]
    public class LevelData : ScriptableObject
    {
        [Header("Level Info")]
        [SerializeField]
        private int number;
        [SerializeField]
        int highestScore;
        [SerializeField]
        string sceneName;
        [SerializeField]
        float timeLimit;
        [SerializeField]
        string instructionsSceneName;
        [SerializeField]
        List<ViolationData> violationsEnabled;
        [SerializeField]
        List<ToolData> toolsEnabled;

        public List<ViolationData> VIOLATIONS_ENABLED { get { return violationsEnabled; } }
        public List<ToolData> TOOLS_ENABLED { get { return toolsEnabled; } }

        public float TIME_LIMIT { get { return timeLimit; } }
        public int LEVEL_NUMBER { get { return number; } }
        public int HIGH_SCORE { get { return highestScore; } set { highestScore = value; } }
        public int SetHighScore { set { highestScore = value; } }
        public string SCENE_NAME { get { return sceneName; } }

        /***************************************/
        [Header("Vehicle Info")]
        public bool AlwaysFollowLights;
        public bool AlwaysFollowNoStopZone;

        [SerializeField]
        List<VEHICLE_STYLE> styles;

        public List<VEHICLE_STYLE> STYLES { get { return styles; } }

        /***************************************/
        [Header("Stars and Unlocks")]
        [SerializeField]
        bool unlocked;
        public int oneStarMinimum;
        public int twoStarMinimum;
        public int threeStarMinimum;
        public LevelData nextLevel;

        public bool IsUnlocked { get { return unlocked; } }
        public void Unlock() { unlocked = true; }
        public void Lock() { unlocked = false; }
        public bool NextLevelUnlocked { get { return highestScore > oneStarMinimum; } }
        public int GetStarsUnlocked { get { return starsUnlocked; } }

        [SerializeField]
        int starsUnlocked
        {
            get
            {
                if (highestScore >= threeStarMinimum)
                    return 3;
                if (highestScore >= twoStarMinimum)
                    return 2;
                if (highestScore >= oneStarMinimum)
                    return 1;

                return 0;
            }
        }
        
        /***************************************/
        [Header("Instructions Info")]
        public List<System.DayOfWeek> allowedDays;
        public bool InstructionsCompleted = false;

        public string INSTRUCTIONS_SCENE_NAME { get { return instructionsSceneName; } }


        /***************************************/

        void Awake()
        {
            highestScore = 0;
            if(number != 1)
                unlocked = false;
        }

    }
}
