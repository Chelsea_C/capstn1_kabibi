﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Urbana
{
    [System.Serializable]
    public class VehicleStyleMinimum
    {
        public VEHICLE_STYLE style;
        public float minimum;
    }

    [CreateAssetMenu(fileName = "New GameData", menuName = "ScriptableObject/GameData")]
    public class GameData : ScriptableObject
    {
        public List<LevelData> LEVELS;
        public int CURRENT_LEVEL_INDEX;

        [Header("Options")]
        public bool AllowEdgeScrolling = false;
        public bool MuteMusic = false;
        public bool MuteSFX = false;

        [Header("Audio")]
        public AudioClip mouseClick;
        //public AudioClip timeAlmostUp;
        public AudioClip correctTicket;
        public AudioClip incorrectTicket;
        public List<AudioClip> BGM_List;
        public AudioClip BGM_menu;
        public AudioClip BG_ambience;
        //public AudioClip buttonSelect;
        public AudioClip buttonHover;
        public AudioClip levelEnd;

        [Header("Violation Data")]
        public ViolationData obstruction;
        public ViolationData coding;
        public ViolationData disregardingSigns;

        [Header("Color Coding")]
        public List<CodingScheme> colorCoding;

        [Header("Vehicle Style")]
        public List<VehicleStyleMinimum> styleMinimums;

        [Header("Tools")]
        public Action[] tools;

        [Header("Timescale")]
        public float speedUpMultiplier;
        public float slowDownSpeed;

        [Header("Uncheck to Reset")]
        [SerializeField]
        bool initialized = false;
        public void Initialize()
        {
            if (!initialized)
            {
                Debug.Log("Resetting LevelDatas");
                CURRENT_LEVEL_INDEX = 1;
                foreach(LevelData ldata in LEVELS)
                {
                    ldata.HIGH_SCORE = 0;

                    if (ldata.LEVEL_NUMBER == CURRENT_LEVEL_INDEX)
                        ldata.Unlock();
                    else
                        ldata.Lock();

                    ldata.InstructionsCompleted = false;
                }
                initialized = true;
            }
            return;
        }
    }
}