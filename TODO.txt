-DOUBLE CHECK EACH LEVEL SETUP
- obstacle check for ped crossing next to intersection (Bool to check if crossing is right before intersection)
- remake radial menu
- option to hide instruction panels
- Days changing?
- Pause game when out of focus?
- more immediate obstruction check
- ghost cars after getting ticketed? or change vehicle_style to saint?


THOUGHTS
- indicators for size of the map (arrows on edges maybe)
- color blind mode
- subtract missed violators/cars that leave the area
- possible HUD elements
	- move timer to HUD
	- move day to HUD
	- move score to HUD
	- toggle for show/hide license plates?
	- toggle for show/hide instruction panels
	- button to show tutorial again if available

BUGS
> level end screen
> when ticketing a car near edge, cant see radial menu
> some cars go through slower cars

CHEATS
> ~/` + 1 : unlock all levels
> ~/` + 2 : Add 1 points
> ~/` + 3 : Add 10 points
> ~/` + RightArrow : time x3 speed
> ~/` + LeftArrow : time x.5 speed
> ~/` + DownArrow : time x1 speed


MOSTLY DONE:


 // EDITED ObstacleChecker component on Vehicle prefab

DONE:
- add incorrect ticket sound
- how to deal with traffic jams
- end screen/time's up sound
- instructions for Disregarding traffic signs
- Speed up time?
- level manager - cars according to color
- game manager - levels unlocking
- camera - bounds
- instructions for Obstruction
- instructions for controls
- make car behavior more tunable - pick which violations they may or may not commit
- automate ui for tools
- audio
- add hint to coding scheme instructions about colors!
- add warning about losing level progress if exiting to menu in the middle of it