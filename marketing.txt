rules - schools, tools, cools
traffic - graphic
road - code, mode, showed, slowed, owed, load, towed, explode


MINI METRO: "Build a better subway"
PLAGUE INC: "Can you infect the world?"

The road less travelled
Enforce the rules less followed
For rules less followed

A bad rule, is no rule
("Lex malla, lex nulla. A bad law is no law" - Cassandra Clare, Lady Midnight)

A game with zero exceptions to the rules.
All the rules. Zero exceptions.

Rules means order

Enforce the rules
Traffic rules and zero exceptions

DEUS EX: HUMAN REVOLUTION: "The truth will change you"
The rules will change you

POKEMON: "Gotta catch 'em all!"
Gotta ticket 'em all

ASURA'S WRATH: "Rage never dies"
Rules never die?

EA SPORTS: "It's in the game"
It's in the rules.

EA GAMES: "Challenge everything"
Enforce everything.

NVIDIA: "The way it's meant to be played."

KINECT: "Better with Kinect", "You are the controller"
Better with rules.

GOOGLE: "Don't be evil"
ALPHABET: "Do the right thing"
APPLE: "Think Different"

LAYS: "Betcha can't eat just one"
Betcha can't ticket just one.

NIKE: "Just do it."

BMW: "Joy is BMW", "Sheer Driving Pleasure", "Designed for Driving Pleasure", "The Ultimate Driving Machine"
Designed for Ticketing Rule-Breakers.
The Ultimate Ticketing Sim.
The Ultimate Traffic Enforcement Sim.

TAG HEUER: "Success. It's a Mind Game."
Car flow and road safety. It's a rules game.

It's all in the rules.